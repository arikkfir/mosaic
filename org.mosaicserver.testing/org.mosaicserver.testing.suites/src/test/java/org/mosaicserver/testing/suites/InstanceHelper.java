package org.mosaicserver.testing.suites;

import com.insightfullogic.lambdabehave.Description;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.mosaicserver.launch.Instance;
import org.mosaicserver.util.Nonnull;

import static java.lang.management.ManagementFactory.getRuntimeMXBean;
import static java.util.Arrays.stream;
import static org.mosaicserver.launch.DeploymentInventory.moduleFiles;

/**
 * @author arik
 */
public final class InstanceHelper
{
    @Nonnull
    public static Instance create( @Nonnull String... moduleNames )
    {
        return new Instance( Stream.of( moduleNames ).map( InstanceHelper::findModule ).collect( moduleFiles() ) ).start();
    }

    @Nonnull
    public static Instance create( @Nonnull Description it, @Nonnull String... moduleNames )
    {
        // create server
        Instance instance = new Instance( Stream.of( moduleNames ).map( InstanceHelper::findModule ).collect( moduleFiles() ) );

        // setup and tear-down
        it.isSetupWith( instance::start );
        it.isConcludedWith( instance::stop );

        return instance;
    }

    @Nonnull
    private static Path findModule( @Nonnull String name )
    {
        Stream<String> classPathStream;
        if( InstanceHelper.class.getClassLoader() instanceof URLClassLoader )
        {
            URLClassLoader urlClassLoader = ( URLClassLoader ) InstanceHelper.class.getClassLoader();
            classPathStream = stream( urlClassLoader.getURLs() ).map( URL::getPath );
        }
        else
        {
            classPathStream = stream( getRuntimeMXBean().getClassPath().split( File.pathSeparator ) );
        }

        String modulePathName = name.matches( "m\\d+" ) ? "org.mosaicserver.testing.modules." + name : name;
        return classPathStream
                .filter( path -> path.endsWith( modulePathName + ".jar" ) || path.contains( "/" + modulePathName + "/" ) )
                .findFirst()
                .map( Paths::get )
                .orElseThrow( () -> new IllegalStateException( "could not find module '" + name + "'" ) );
    }
}
