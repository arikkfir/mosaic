package org.mosaicserver.testing.suites;

import com.insightfullogic.lambdabehave.JunitSuiteRunner;
import java.lang.reflect.Field;
import org.junit.runner.RunWith;
import org.mosaicserver.launch.Instance;
import org.mosaicserver.modules.Module;
import org.mosaicserver.testing.modules.m03.M03Component;

import static com.insightfullogic.lambdabehave.Suite.describe;
import static org.mosaicserver.testing.suites.InstanceHelper.create;
import static org.mosaicserver.testing.suites.Predicates.modulePredicate;

/**
 * @author arik
 */
@RunWith( JunitSuiteRunner.class )
public class ShutdownSpec
{
    {
        describe( "a server", it -> {

            it.should( "deploy m03", expect -> {

                Field creationsField, shutdownsField;
                try( Instance instance = create( "m03" ) )
                {
                    Module module = instance.getModuleManager().getModules( modulePredicate( "m03" ) ).iterator().next();
                    Class<?> componentClass = module.getClass( M03Component.class.getName() ).get();

                    creationsField = componentClass.getField( "creations" );
                    expect.that( creationsField.get( null ) ).isEqualTo( 1 );

                    shutdownsField = componentClass.getField( "shutdowns" );
                    expect.that( shutdownsField.get( null ) ).isEqualTo( 0 );

                }

                expect.that( shutdownsField.get( null ) ).isEqualTo( 1 );

            } );

        } );
    }
}
