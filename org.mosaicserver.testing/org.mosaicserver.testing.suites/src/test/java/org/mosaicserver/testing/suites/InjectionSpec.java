package org.mosaicserver.testing.suites;

import com.insightfullogic.lambdabehave.JunitSuiteRunner;
import java.lang.reflect.Field;
import org.junit.runner.RunWith;
import org.mosaicserver.launch.Instance;
import org.mosaicserver.modules.Module;
import org.mosaicserver.testing.modules.m01.M01Component;
import org.mosaicserver.testing.modules.m02.M02Component;

import static com.insightfullogic.lambdabehave.Suite.describe;
import static org.mosaicserver.testing.suites.InstanceHelper.create;
import static org.mosaicserver.testing.suites.Predicates.modulePredicate;

/**
 * @author arik
 */
@SuppressWarnings( "CodeBlock2Expr" )
@RunWith( JunitSuiteRunner.class )
public class InjectionSpec
{
    {
        describe( "a server", it -> {

            it.should( "inject m01 into m02", expect -> {
                try( Instance instance = create( "m01", "m02" ) )
                {
                    Module m01Module = instance.getModuleManager().getModules( modulePredicate( "m01" ) ).iterator().next();
                    Module m02Module = instance.getModuleManager().getModules( modulePredicate( "m02" ) ).iterator().next();

                    Class<?> m01ComponentClass = m01Module.getClass( M01Component.class.getName() ).get();
                    Class<?> m02ComponentClass = m02Module.getClass( M02Component.class.getName() ).get();

                    Object m01ComponentInstance = instance.getComponentManager().getComponent( m01ComponentClass ).get();
                    Object m02ComponentInstance = instance.getComponentManager().getComponent( m02ComponentClass ).get();

                    Field injectedFieldInM02Class = m02ComponentClass.getField( "m01Component" );
                    Object injectedM01Instance = injectedFieldInM02Class.get( m02ComponentInstance );
                    expect.that( injectedM01Instance ).sameInstance( m01ComponentInstance );
                }
            } );

        } );
    }
}
