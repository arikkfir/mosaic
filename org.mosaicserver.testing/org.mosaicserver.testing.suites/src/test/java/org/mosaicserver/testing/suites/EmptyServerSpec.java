package org.mosaicserver.testing.suites;

import com.insightfullogic.lambdabehave.JunitSuiteRunner;
import org.junit.runner.RunWith;
import org.mosaicserver.launch.Instance;

import static com.insightfullogic.lambdabehave.Suite.describe;
import static org.mosaicserver.testing.suites.InstanceHelper.create;

/**
 * @author arik
 */
@RunWith( JunitSuiteRunner.class )
public class EmptyServerSpec
{
    {
        describe( "an empty server", it -> {

            Instance instance = create( it );
            it.should( "have a module manager", expect -> expect.that( instance.getModuleManager() ).isNotNull() );
            it.should( "have a component manager", expect -> expect.that( instance.getComponentManager() ).isNotNull() );
            it.should( "have no modules", expect -> expect.that( instance.getModuleManager().getModules().count() ).is( 0l ) );

        } );
    }
}
