package org.mosaicserver.testing.suites;

import java.util.function.Predicate;
import java.util.regex.Pattern;
import org.mosaicserver.modules.CoordinatesPredicate;
import org.mosaicserver.modules.Module;
import org.mosaicserver.util.Nonnull;

import static java.util.regex.Pattern.quote;

/**
 * @author arik
 */
class Predicates
{
    static final CoordinatesPredicate M01_PREDICATE =
            new CoordinatesPredicate( Pattern.quote( "org.mosaicserver.testing.modules" ),
                                      Pattern.quote( "org.mosaicserver.testing.modules.m01" ) );

    static final CoordinatesPredicate M02_PREDICATE =
            new CoordinatesPredicate( Pattern.quote( "org.mosaicserver.testing.modules" ),
                                      Pattern.quote( "org.mosaicserver.testing.modules.m02" ) );

    static final CoordinatesPredicate M01_M02_PREDICATE =
            new CoordinatesPredicate( Pattern.quote( "org.mosaicserver.testing.modules" ),
                                      Pattern.quote( "org.mosaicserver.testing.modules.m" ) + "\\d+" );

    @Nonnull
    static Predicate<Module> modulePredicate( String moduleName )
    {
        return new CoordinatesPredicate( quote( "org.mosaicserver.testing.modules" ),
                                         quote( "org.mosaicserver.testing.modules." + moduleName ) );
    }
}
