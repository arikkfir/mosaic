package org.mosaicserver.testing.suites;

import com.insightfullogic.lambdabehave.JunitSuiteRunner;
import com.insightfullogic.lambdabehave.expectations.Expect;
import org.junit.runner.RunWith;
import org.mosaicserver.launch.Instance;
import org.mosaicserver.modules.Module;
import org.mosaicserver.testing.modules.m01.M01Component;
import org.mosaicserver.testing.modules.m02.M02Component;
import org.mosaicserver.util.Nonnull;

import static com.insightfullogic.lambdabehave.Suite.describe;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mosaicserver.testing.suites.InstanceHelper.create;
import static org.mosaicserver.testing.suites.Predicates.modulePredicate;

/**
 * @author arik
 */
@RunWith( JunitSuiteRunner.class )
public class DeploymentSpec
{
    {
        describe( "a server", it -> {

            it.should( "deploy m01", expect -> {
                try( Instance instance = create( "m01" ) )
                {
                    checkModule( instance, expect, "m01", M01Component.class.getName() );
                }
            } );

            it.should( "deploy m01 and m02", expect -> {
                try( Instance instance = create( "m01", "m02" ) )
                {
                    checkModule( instance, expect, "m01", M01Component.class.getName() );
                    checkModule( instance, expect, "m02", M02Component.class.getName() );
                }
            } );

        } );
    }

    private void checkModule( @Nonnull Instance instance,
                              @Nonnull Expect expect,
                              @Nonnull String moduleName,
                              @Nonnull String componentClassName )
    {
        expect.that( instance.getModuleManager().getModules( modulePredicate( moduleName ) ).count() ).is( 1l );

        Module module = instance.getModuleManager().getModules( modulePredicate( moduleName ) ).iterator().next();
        expect.that( module.getGroupId() ).isEqualTo( "org.mosaicserver.testing.modules" );
        expect.that( module.getArtifactId() ).isEqualTo( "org.mosaicserver.testing.modules." + moduleName );
        expect.that( module.getVersion() ).isEqualTo( instance.getVersion() );

        Class<?> componentClass = module.getClass( componentClassName ).get();
        expect.that( componentClass.getName() ).isEqualTo( componentClassName );

        Object componentInstance = instance.getComponentManager().getComponent( componentClass ).get();
        expect.that( componentInstance ).is( instanceOf( componentClass ) );
    }
}
