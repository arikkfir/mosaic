package org.mosaicserver.testing.modules.m03;

import org.mosaicserver.components.Component;
import org.mosaicserver.components.PreShutdown;

/**
 * @author arik
 */
@Component
public class M03Component
{
    public static volatile int creations = 0;

    public static volatile int shutdowns = 0;

    public M03Component()
    {
        creations++;
    }

    @PreShutdown
    private void destroy()
    {
        shutdowns++;
    }
}
