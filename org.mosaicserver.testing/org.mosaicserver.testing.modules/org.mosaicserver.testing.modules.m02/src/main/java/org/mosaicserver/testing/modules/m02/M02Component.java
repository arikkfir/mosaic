package org.mosaicserver.testing.modules.m02;

import org.mosaicserver.components.Component;
import org.mosaicserver.testing.modules.m01.M01Component;

/**
 * @author arik
 */
@Component
public class M02Component
{
    @Component
    public M01Component m01Component;
}
