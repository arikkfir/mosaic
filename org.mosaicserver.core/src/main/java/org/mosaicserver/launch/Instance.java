package org.mosaicserver.launch;

import java.net.URL;
import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import org.mosaicserver.components.impl.ComponentManagerImpl;
import org.mosaicserver.modules.impl.ModuleManagerImpl;
import org.mosaicserver.util.*;
import org.mosaicserver.weaving.impl.ApplicationClassLoader;
import org.slf4j.Logger;

import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.stream.Collectors.toList;
import static org.mosaicserver.util.Format.msg;
import static org.mosaicserver.util.StringUtil.rightPad;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 */
public class Instance implements AutoCloseable
{
    private static final Logger BOOT_LOG = getLogger( "boot" );

    private static final String EMPTY_LOGO_LINE = StringUtil.repeat( " ", 34 );

    @Nonnull
    private final Version version;

    @Nonnull
    private final Path workDir;

    private final boolean tempWorkDir;

    @Nonnull
    private final ModuleManagerImpl moduleManager;

    @Nonnull
    private final ComponentManagerImpl componentManager;

    @Nullable
    private ZonedDateTime startupTime;

    @Nullable
    private ApplicationClassLoader applicationClassLoader;

    public Instance( @Nonnull DeploymentInventory deploymentInventory )
    {
        this( IoUtil.createTempDirectory( "org.mosaicserver-" ), true, deploymentInventory );
    }

    public Instance( @Nonnull Path workDir, @Nonnull DeploymentInventory deploymentInventory )
    {
        this( workDir, false, deploymentInventory );
    }

    private Instance( @Nonnull Path workDir,
                      boolean workDirTemporary,
                      @Nonnull DeploymentInventory deploymentInventory )
    {
        URL pomPropertiesResource = getClass().getClassLoader().getResource( "META-INF/maven/org.mosaicserver/org.mosaicserver.core/pom.properties" );
        if( pomPropertiesResource == null )
        {
            this.version = Version.valueOf( "0.0.0" );
        }
        else
        {
            Properties pomProperties = IoUtil.readProperties( pomPropertiesResource );
            this.version = Version.valueOf( pomProperties.getProperty( "version", "0.0.0" ) );
        }

        this.workDir = workDir;
        this.tempWorkDir = workDirTemporary;
        this.moduleManager = new ModuleManagerImpl( this, deploymentInventory );
        this.componentManager = new ComponentManagerImpl( this );
    }

    @Override
    public String toString()
    {
        return ToStringHelper.create( this ).toString();
    }

    @Nonnull
    public final Instance start()
    {
        this.startupTime = ZonedDateTime.now();

        // print server info
        printStartupHeader();

        this.moduleManager.start();
        this.applicationClassLoader = new ApplicationClassLoader( getClass().getClassLoader(), this );
        this.componentManager.start();

        return this;
    }

    public final void stop()
    {
        this.componentManager.stop();
        this.moduleManager.stop();
        this.applicationClassLoader = null;

        printShutdownFooter();
        this.startupTime = null;

        if( this.tempWorkDir )
        {
            IoUtil.delete( this.workDir );
        }
    }

    @Override
    public final void close() throws Exception
    {
        stop();
    }

    public final Version getVersion()
    {
        return this.version;
    }

    @Nonnull
    public final Path getWorkDir()
    {
        return this.workDir;
    }

    @Nonnull
    public final ApplicationClassLoader getApplicationClassLoader()
    {
        if( this.applicationClassLoader == null )
        {
            throw new IllegalStateException( "server not started" );
        }
        else
        {
            return this.applicationClassLoader;
        }
    }

    @Nonnull
    public final ModuleManagerImpl getModuleManager()
    {
        return this.moduleManager;
    }

    @Nonnull
    public final ComponentManagerImpl getComponentManager()
    {
        return this.componentManager;
    }

    private void printStartupHeader()
    {
        // load logo & create header lines
        List<String> logoLines = IoUtil.readLines( Main.class.getResource( "logo.txt" ) )
                                       .stream()
                                       .map( line -> rightPad( line, ' ', 34 ) )
                                       .collect( toList() );

        List<String> infoLines = new LinkedList<>();
        infoLines.add( msg( "Mosaic server version....{}", this.version ) );
        infoLines.add( msg( "---------------------------------------------------------------------------------------" ) );
        infoLines.add( msg( "Work directory...........{}", this.workDir ) );

        // print inventory, if any
        DeploymentInventory inventory = this.moduleManager.getDeploymentInventory();
        infoLines.addAll( inventory.getModuleDirectories().stream().map( path -> msg( "Modules directory........{}", path ) ).collect( toList() ) );
        infoLines.addAll( inventory.getModuleFiles().stream().map( path -> msg( "Module file..............{}", path ) ).collect( toList() ) );
        infoLines.addAll( inventory.getModuleInventoryFiles().stream().map( path -> msg( "Module inventory file....{}", path ) ).collect( toList() ) );

        // print while merging every logo+info line
        BOOT_LOG.info( "" );
        BOOT_LOG.info( "********************************************************************************************************************" );
        Iterator<String> logoIt = logoLines.iterator(), infoIt = infoLines.iterator();
        while( logoIt.hasNext() || infoIt.hasNext() )
        {
            BOOT_LOG.info( ( logoIt.hasNext() ? logoIt.next() : EMPTY_LOGO_LINE ) + ( infoIt.hasNext() ? infoIt.next() : "" ) );
        }
        BOOT_LOG.info( "********************************************************************************************************************" );
        BOOT_LOG.info( "" );
    }

    private void printShutdownFooter()
    {
        BOOT_LOG.info( "" );
        BOOT_LOG.info( "********************************************************************************************************************" );

        ZonedDateTime shutdownTime = ZonedDateTime.now();
        ZonedDateTime startupTime = this.startupTime;
        if( startupTime != null )
        {
            BOOT_LOG.info( "Mosaic server has been shutdown (up-time was {} seconds)", startupTime.until( shutdownTime, SECONDS ) );
        }
        else
        {
            BOOT_LOG.info( "Mosaic server has been shutdown" );
        }
        BOOT_LOG.info( "********************************************************************************************************************" );
        BOOT_LOG.info( "" );
    }
}
