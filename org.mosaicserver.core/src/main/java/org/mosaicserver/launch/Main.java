package org.mosaicserver.launch;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.mosaicserver.util.IoUtil;
import org.mosaicserver.util.ParsedCommandLine;

import static java.util.stream.Collectors.joining;

/**
 * @author arik
 */
public final class Main
{
    public static void main( String[] args ) throws IOException
    {
        // parse command line arguments
        ParsedCommandLine commandLine = new ParsedCommandLine( Stream.of( args ).collect( joining( " " ) ) );

        // obtain work directory (clean it if requested to, usually during development)
        Path workDir = Paths.get( commandLine.singleValued( "work", "w" ).orElse( "work" ) );
        if( commandLine.hasOption( "clean", "c" ) )
        {
            IoUtil.delete( workDir );
        }

        // discover modules
        DeploymentInventory inventory = new DeploymentInventory();
        commandLine.multiValued( "dir", "d" ).stream().map( Paths::get ).forEach( inventory::addModuleDirectory );
        commandLine.multiValued( "module", "m" ).stream().map( Paths::get ).forEach( inventory::addModuleFile );
        commandLine.multiValued( "inventory", "i" ).stream().map( Paths::get ).forEach( inventory::addModuleInventoryFile );

        // start the server
        new Instance( workDir, inventory ).start();
    }
}
