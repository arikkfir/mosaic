package org.mosaicserver.launch;

import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collector;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.ToStringHelper;

/**
 * @author arik
 */
public class DeploymentInventory
{
    @Nonnull
    public static Collector<Path, List<Path>, DeploymentInventory> moduleFiles()
    {
        return Collector.of(
                LinkedList::new,
                Collection::add, (
                        paths1,
                        paths2 ) -> {
                    paths1.addAll( paths2 );
                    return paths1;
                },
                paths -> {
                    DeploymentInventory inventory = new DeploymentInventory();
                    paths.forEach( inventory::addModuleFile );
                    return inventory;
                },
                Collector.Characteristics.UNORDERED );
    }

    @Nonnull
    public static Collector<Path, List<Path>, DeploymentInventory> moduleDirectories()
    {
        return Collector.of(
                LinkedList::new,
                Collection::add, (
                        paths1,
                        paths2 ) -> {
                    paths1.addAll( paths2 );
                    return paths1;
                },
                paths -> {
                    DeploymentInventory inventory = new DeploymentInventory();
                    paths.forEach( inventory::addModuleDirectory );
                    return inventory;
                },
                Collector.Characteristics.UNORDERED );
    }

    @Nonnull
    public static Collector<Path, List<Path>, DeploymentInventory> moduleInventories()
    {
        return Collector.of(
                LinkedList::new,
                Collection::add, (
                        paths1,
                        paths2 ) -> {
                    paths1.addAll( paths2 );
                    return paths1;
                },
                paths -> {
                    DeploymentInventory inventory = new DeploymentInventory();
                    paths.forEach( inventory::addModuleInventoryFile );
                    return inventory;
                },
                Collector.Characteristics.UNORDERED );
    }

    @Nonnull
    private final List<Path> moduleDirectories = new LinkedList<>();

    @Nonnull
    private final List<Path> moduleFiles = new LinkedList<>();

    @Nonnull
    private final List<Path> moduleInventoryFiles = new LinkedList<>();

    @Override
    public String toString()
    {
        return ToStringHelper.create( this )
                             .add( "moduleDirs", this.moduleDirectories )
                             .add( "moduleFiles", this.moduleFiles )
                             .add( "inventoryFiles", this.moduleInventoryFiles )
                             .toString();
    }

    @Nonnull
    public List<Path> getModuleDirectories()
    {
        return this.moduleDirectories;
    }

    @Nonnull
    public DeploymentInventory addModuleDirectory( @Nonnull Path path )
    {
        this.moduleDirectories.add( path );
        return this;
    }

    @Nonnull
    public List<Path> getModuleFiles()
    {
        return this.moduleFiles;
    }

    @Nonnull
    public DeploymentInventory addModuleFile( @Nonnull Path path )
    {
        this.moduleFiles.add( path );
        return this;
    }

    @Nonnull
    public List<Path> getModuleInventoryFiles()
    {
        return this.moduleInventoryFiles;
    }

    @Nonnull
    public DeploymentInventory addModuleInventoryFile( @Nonnull Path path )
    {
        this.moduleInventoryFiles.add( path );
        return this;
    }
}
