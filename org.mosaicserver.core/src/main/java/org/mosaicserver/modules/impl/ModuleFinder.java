package org.mosaicserver.modules.impl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.mosaicserver.launch.Instance;
import org.mosaicserver.util.IoUtil;
import org.mosaicserver.util.Nonnull;

/**
 * @author arik
 */
class ModuleFinder
{
    @Nonnull
    private final Instance instance;

    @Nonnull
    private final List<ModuleImpl> modules = new LinkedList<>();

    ModuleFinder( @Nonnull Instance instance )
    {
        this.instance = instance;
    }

    void addModulesDirectory( @Nonnull Path path )
    {
        IoUtil.walk( path )
              .filter( Files::isRegularFile )
              .filter( Files::isReadable )
              .filter( file -> file.getFileName().toString().toLowerCase().endsWith( ".jar" ) )
              .map( file -> new ModuleImpl( this.instance, file ) )
              .forEach( this.modules::add );
    }

    void addModuleJar( @Nonnull Path path )
    {
        this.modules.add( new ModuleImpl( this.instance, path ) );
    }

    void addInventoryFile( @Nonnull Path path )
    {
        IoUtil.readLines( path ).stream().map( Paths::get ).forEach( this::addModuleJar );
    }

    @Nonnull
    List<ModuleImpl> getModules()
    {
        return Collections.unmodifiableList( this.modules );
    }
}
