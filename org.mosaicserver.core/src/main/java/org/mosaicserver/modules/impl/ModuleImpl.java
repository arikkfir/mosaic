package org.mosaicserver.modules.impl;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.attribute.FileTime;
import java.util.Collection;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.mosaicserver.launch.Instance;
import org.mosaicserver.modules.Module;
import org.mosaicserver.util.IoUtil;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.ToStringHelper;
import org.mosaicserver.util.Version;

/**
 * @author arik
 */
public final class ModuleImpl implements Module
{
    @Nonnull
    private final Instance instance;

    @Nonnull
    private final String groupId;

    @Nonnull
    private final String artifactId;

    @Nonnull
    private final Version version;

    @Nonnull
    private final Path originalPath;

    @Nonnull
    private final Path explodedPath;

    public ModuleImpl( @Nonnull Instance instance, @Nonnull Path path )
    {
        this.instance = instance;
        this.originalPath = path;

        if( Files.isDirectory( this.originalPath ) )
        {
            this.explodedPath = this.originalPath;
        }
        else
        {
            String fileName = this.originalPath.getFileName().toString();
            if( fileName.endsWith( ".jar" ) )
            {
                fileName = fileName.substring( 0, fileName.length() - ".jar".length() );
            }
            this.explodedPath = this.instance.getWorkDir().resolve( composeWorkName( fileName ) );

            if( Files.exists( this.explodedPath ) )
            {
                if( Files.isRegularFile( this.explodedPath ) )
                {
                    IoUtil.delete( this.explodedPath );
                }
                else
                {
                    FileTime sourceModTime = IoUtil.getLastModifiedTime( this.originalPath );
                    IoUtil.walk( this.explodedPath )
                          .filter( Files::isRegularFile )
                          .filter( p -> IoUtil.getLastModifiedTime( this.explodedPath ).compareTo( sourceModTime ) > 0 )
                          .findFirst()
                          .ifPresent( p -> IoUtil.delete( this.explodedPath ) );
                }
            }

            if( !Files.exists( this.explodedPath ) )
            {
                IoUtil.extract( this.originalPath, this.explodedPath );
            }
        }

        Collection<Path> pomPropertiesFiles = findResources( "META-INF/maven/*/*/pom.properties" );
        if( pomPropertiesFiles.isEmpty() )
        {
            this.groupId = "unknown";
            this.artifactId = "unknown";
            this.version = Version.valueOf( "0.0.0-SNAPSHOT" );
        }
        else
        {
            Properties pomProperties = IoUtil.readProperties( pomPropertiesFiles.iterator().next() );
            this.groupId = pomProperties.getProperty( "groupId" );
            this.artifactId = pomProperties.getProperty( "artifactId" );
            this.version = Version.valueOf( pomProperties.getProperty( "version" ) );
        }
    }

    @Override
    public String toString()
    {
        return ToStringHelper.create( this )
                             .add( "groupId", this.groupId )
                             .add( "artifactId", this.artifactId )
                             .add( "version", this.version )
                             .toString();
    }

    @Nonnull
    @Override
    public String getGroupId()
    {
        return this.groupId;
    }

    @Nonnull
    @Override
    public String getArtifactId()
    {
        return this.artifactId;
    }

    @Nonnull
    @Override
    public Version getVersion()
    {
        return this.version;
    }

    @Nonnull
    @Override
    public Path getOriginalPath()
    {
        return this.originalPath;
    }

    @Nonnull
    @Override
    public Path getExplodedPath()
    {
        return this.explodedPath;
    }

    @Nonnull
    @Override
    public Optional<Path> findResource( @Nonnull String pattern )
    {
        String ptrn = pattern;
        if( !ptrn.startsWith( "glob:" ) && !ptrn.startsWith( "regex:" ) )
        {
            ptrn = "glob:" + ptrn;
        }

        PathMatcher pathMatcher = this.explodedPath.getFileSystem().getPathMatcher( ptrn );
        try
        {
            return Files.walk( this.explodedPath )
                        .map( this.explodedPath::relativize )
                        .filter( pathMatcher::matches ).findFirst();
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    @Nonnull
    @Override
    public Collection<Path> findResources( @Nonnull String pattern )
    {
        String ptrn = pattern;
        if( !ptrn.startsWith( "glob:" ) && !ptrn.startsWith( "regex:" ) )
        {
            ptrn = "glob:" + ptrn;
        }

        PathMatcher pathMatcher = this.explodedPath.getFileSystem().getPathMatcher( ptrn );
        try
        {
            return Files.walk( this.explodedPath )
                        .filter( path -> pathMatcher.matches( this.explodedPath.relativize( path ) ) )
                        .collect( Collectors.toList() );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    @Nonnull
    @Override
    public Optional<? extends Class<?>> getClass( @Nonnull String className )
    {
        return getClasses().filter( clazz -> clazz.getName().equals( className ) ).findFirst();
    }

    @Nonnull
    @Override
    public Stream<? extends Class<?>> getClasses()
    {
        return findResources( "**/*.class" )
                .stream()
                .map( this.explodedPath::relativize )
                .map( this::pathToClassName )
                .map( className -> {
                    try
                    {
                        return this.instance.getApplicationClassLoader().loadClass( className );
                    }
                    catch( ClassNotFoundException e )
                    {
                        //noinspection ConstantConditions
                        return null;
                    }
                } )
                .filter( clazz -> clazz != null );
    }

    @Nonnull
    private String composeWorkName( @Nonnull String name )
    {
        return name.replace( File.separatorChar, '_' );
    }

    @Nonnull
    private String pathToClassName( @Nonnull Path path )
    {
        String pathName = path.toString();
        if( pathName.toLowerCase().endsWith( ".class" ) )
        {
            pathName = pathName.substring( 0, pathName.length() - ".class".length() );
        }
        return pathName.replace( '/', '.' );
    }
}
