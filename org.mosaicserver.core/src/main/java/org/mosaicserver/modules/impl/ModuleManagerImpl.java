package org.mosaicserver.modules.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.mosaicserver.launch.DeploymentInventory;
import org.mosaicserver.launch.Instance;
import org.mosaicserver.modules.Module;
import org.mosaicserver.modules.ModuleManager;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.Nullable;
import org.mosaicserver.util.ToStringHelper;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 */
public class ModuleManagerImpl implements ModuleManager
{
    private static final Logger LOG = getLogger( ModuleManagerImpl.class );

    @Nonnull
    private final Instance instance;

    @Nonnull
    private final DeploymentInventory deploymentInventory;

    @Nullable
    private List<ModuleImpl> modules;

    public ModuleManagerImpl( @Nonnull Instance instance, @Nonnull DeploymentInventory inventory )
    {
        this.instance = instance;
        this.deploymentInventory = inventory;
    }

    @Override
    public String toString()
    {
        return ToStringHelper.create( this )
                             .add( "inventory", this.deploymentInventory )
                             .toString();
    }

    @Nonnull
    public DeploymentInventory getDeploymentInventory()
    {
        return this.deploymentInventory;
    }

    public void start()
    {
        ModuleFinder moduleFinder = new ModuleFinder( this.instance );
        this.deploymentInventory.getModuleDirectories().forEach( moduleFinder::addModulesDirectory );
        this.deploymentInventory.getModuleFiles().forEach( moduleFinder::addModuleJar );
        this.deploymentInventory.getModuleInventoryFiles().forEach( moduleFinder::addInventoryFile );
        this.modules = moduleFinder.getModules();
        this.modules.forEach( module -> LOG.info( "Discovered module '{}:{}:{}' at '{}'", module.getGroupId(), module.getArtifactId(), module.getVersion(), module.getOriginalPath() ) );
    }

    public void stop()
    {
        this.modules = null;
    }

    @Nonnull
    @Override
    public Stream<? extends Module> getModules()
    {
        List<ModuleImpl> modules = this.modules;
        if( modules != null )
        {
            return new LinkedList<>( modules ).stream();
        }
        else
        {
            throw new IllegalStateException( "server not started" );
        }
    }

    @Nonnull
    @Override
    public Stream<? extends Module> getModules( @Nonnull Predicate<Module> predicate )
    {
        return getModules().filter( predicate );
    }
}
