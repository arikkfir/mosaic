package org.mosaicserver.weaving.impl.intercept;

import org.mosaicserver.util.Nonnull;
import org.mosaicserver.weaving.MethodInterceptor;

import static java.util.Arrays.copyOf;

/**
* @author arik
*/
class AfterMethodExceptionImpl extends AbstractMethodInvocation
        implements MethodInterceptor.AfterMethodException
{
    @Nonnull
    private final Object[] arguments;

    AfterMethodExceptionImpl( @Nonnull InvocationContext context )
    {
        super( context );
        Object[] arguments = this.context.getArguments();
        this.arguments = copyOf( arguments, arguments.length );
    }

    @Nonnull
    @Override
    public Object[] getArguments()
    {
        return this.arguments;
    }

    @Nonnull
    @Override
    public Throwable getThrowable()
    {
        Throwable throwable = this.context.getThrowable();
        if( throwable == null )
        {
            throw new IllegalStateException( "No throwable found" );
        }
        else
        {
            return throwable;
        }
    }
}
