package org.mosaicserver.weaving.impl.intercept;

import java.lang.reflect.Method;
import java.util.*;
import org.mosaicserver.components.impl.ComponentManagerImpl;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.Nullable;
import org.mosaicserver.weaving.MethodInterceptor;
import org.mosaicserver.weaving.impl.ApplicationClassLoader;
import org.slf4j.Logger;

import static org.mosaicserver.util.Format.msg;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 */
public final class MethodEntry
{
    private static final Logger LOG = getLogger( MethodEntry.class );

    @Nonnull
    static final MethodInterceptor.BeforeInvocationDecision continueDecision = new ContinueDecision();

    @Nonnull
    static final MethodInterceptor.BeforeInvocationDecision abortDecision = new AbortDecision();

    @Nonnull
    private final Class<?> declaringClass;

    @Nonnull
    private final String methodName;

    @Nonnull
    private final Class<?>[] parameterTypes;

    @Nonnull
    private final String methodNotFoundMessage;

    @Nonnull
    private final ThreadLocal<Deque<InvocationContext>> contextHolder = new ThreadLocal<Deque<InvocationContext>>()
    {
        @Override
        protected Deque<InvocationContext> initialValue()
        {
            return new LinkedList<>();
        }
    };

    @Nullable
    private Optional<Method> methodHolder;

    @Nullable
    private List<InterceptorRef> interceptors;

    public MethodEntry( @Nonnull Class<?> declaringClass,
                        @Nonnull String methodName,
                        @Nonnull Class<?>[] parameterTypes )
    {
        this.declaringClass = declaringClass;
        this.methodName = methodName;
        this.parameterTypes = parameterTypes;
        this.methodNotFoundMessage = msg( "could not find method '{}({})' in '{}'",
                                          this.methodName,
                                          Arrays.asList( this.parameterTypes ),
                                          this.declaringClass.getName() );
    }

    public boolean beforeInvocation( @Nullable Object target, @Nonnull Object[] arguments )
    {
        // create context for method invocation and push to stack
        InvocationContext context = new InvocationContext( this, target, arguments );
        this.contextHolder.get().push( context );

        // invoke interceptors "before" action, returning true if method should proceed, false if circumvent method and request a call to "after" action
        return context.beforeInvocation();
    }

    @Nullable
    public Object afterAbortedInvocation() throws Throwable
    {
        return this.contextHolder.get().peek().afterInvocation();
    }

    @Nullable
    public Object afterThrowable( @Nonnull Throwable throwable ) throws Throwable
    {
        InvocationContext context = this.contextHolder.get().peek();
        context.setThrowable( throwable );
        return context.afterInvocation();
    }

    @Nullable
    public Object afterSuccessfulInvocation( @Nonnull Object result ) throws Throwable
    {
        InvocationContext context = this.contextHolder.get().peek();
        context.setReturnValue( result );
        return context.afterInvocation();
    }

    public void cleanup()
    {
        Deque<InvocationContext> deque = this.contextHolder.get();
        if( deque.isEmpty() )
        {
            LOG.error( "STACK EMPTY! received method: {}", this );
        }
        else if( !deque.peek().getMethodEntry().equals( this ) )
        {
            LOG.error( "STACK DIRTY!\n" +
                       "    On stack: {}\n" +
                       "    Received: {}",
                       deque.peek().getMethodEntry(), this
            );
        }
        else
        {
            deque.pop();
        }
    }

    @Nonnull
    Method getMethod()
    {
        if( this.methodHolder == null )
        {
            synchronized( this )
            {
                if( this.methodHolder == null )
                {
                    try
                    {
                        Method method = this.declaringClass.getDeclaredMethod( this.methodName, this.parameterTypes );
                        this.methodHolder = Optional.of( method );
                    }
                    catch( NoSuchMethodException e )
                    {
                        throw new IllegalStateException( msg( "could not find method '{}({})' in '{}'",
                                                              this.methodName,
                                                              Arrays.asList( this.parameterTypes ),
                                                              this.declaringClass.getName() ),
                                                         e );
                    }
                }
            }
        }
        return this.methodHolder.orElseThrow( () -> new IllegalStateException( this.methodNotFoundMessage ) );
    }

    @Nonnull
    List<InterceptorRef> getMethodInterceptors()
    {
        if( this.interceptors == null )
        {
            synchronized( this )
            {
                if( this.interceptors == null )
                {
                    Method method = getMethod();

                    ApplicationClassLoader appClassLoader = ( ApplicationClassLoader ) method.getDeclaringClass().getClassLoader();
                    ComponentManagerImpl componentManager = appClassLoader.getInstance().getComponentManager();

                    List<InterceptorRef> interceptors = null;
                    for( MethodInterceptor interceptor : componentManager.getComponents( MethodInterceptor.class ) )
                    {
                        Map<String, Object> context = new HashMap<>();
                        if( interceptor.interestedIn( method, context ) )
                        {
                            if( interceptors == null )
                            {
                                interceptors = new LinkedList<>();
                            }
                            interceptors.add( new InterceptorRef( interceptor, context ) );
                        }
                    }

                    this.interceptors = interceptors == null ? Collections.<InterceptorRef>emptyList() : interceptors;
                }
            }
        }
        return this.interceptors;
    }
}
