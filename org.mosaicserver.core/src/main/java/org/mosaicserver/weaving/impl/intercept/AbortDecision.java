package org.mosaicserver.weaving.impl.intercept;

import org.mosaicserver.weaving.MethodInterceptor;

/**
* @author arik
*/
class AbortDecision implements MethodInterceptor.BeforeInvocationDecision
{
}
