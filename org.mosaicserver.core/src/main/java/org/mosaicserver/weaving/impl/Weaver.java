package org.mosaicserver.weaving.impl;

import javassist.*;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.MethodInfo;
import org.mosaicserver.util.Nonnull;

import static org.mosaicserver.util.Format.msg;

/**
 * @author arik
 */
public abstract class Weaver
{
    @Nonnull
    protected final String getParameterName( @Nonnull CtBehavior behavior, int index )
    {
        int parametersCount;
        try
        {
            parametersCount = behavior.getParameterTypes().length;
            if( index >= parametersCount )
            {
                throw new IllegalArgumentException( msg( "behavior '{}' only has {} parameters (cannot obtain name of parameter {})", behavior.toString(), parametersCount, index ) );
            }
        }
        catch( NotFoundException e )
        {
            return "arg" + index;
        }

        MethodInfo methodInfo = behavior.getMethodInfo();
        LocalVariableAttribute table = ( LocalVariableAttribute ) methodInfo.getCodeAttribute().getAttribute( LocalVariableAttribute.tag );

        int numberOfLocalVariables = table.tableLength();
        int parameterIndexInLvt = Modifier.isStatic( behavior.getModifiers() ) ? 0 : 1 + index;
        if( parameterIndexInLvt >= numberOfLocalVariables )
        {
            return "arg" + index;
        }

        return methodInfo.getConstPool().getUtf8Info( table.nameIndex( parameterIndexInLvt ) );
    }

    public abstract void weave( @Nonnull CtClass ctClass )
            throws ClassNotFoundException, NotFoundException, CannotCompileException;
}
