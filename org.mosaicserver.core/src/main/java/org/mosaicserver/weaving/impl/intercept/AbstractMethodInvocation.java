package org.mosaicserver.weaving.impl.intercept;

import java.lang.reflect.Method;
import java.util.Map;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.Nullable;
import org.mosaicserver.weaving.MethodInterceptor;

/**
 * @author arik
 */
abstract class AbstractMethodInvocation implements MethodInterceptor.MethodInvocation
{
    @Nonnull
    protected final InvocationContext context;

    @Nullable
    protected InterceptorRef interceptorRef;

    protected AbstractMethodInvocation( @Nonnull InvocationContext context )
    {
        this.context = context;
    }

    @Nonnull
    @Override
    public final Map<String, Object> getInterceptorContext()
    {
        if( this.interceptorRef == null )
        {
            throw new IllegalStateException( "Interceptor context not set on invocation!" );
        }
        return this.interceptorRef.getContext();
    }

    @Nonnull
    @Override
    public final Map<String, Object> getInvocationContext()
    {
        if( this.interceptorRef == null )
        {
            throw new IllegalStateException( "Interceptor context not set on invocation!" );
        }
        return this.context.getInterceptorInvocationContext( this.interceptorRef );
    }

    @Nonnull
    @Override
    public final Method getMethod()
    {
        return this.context.getMethodEntry().getMethod();
    }

    @Nullable
    @Override
    public final Object getObject()
    {
        return this.context.getObject();
    }
}
