package org.mosaicserver.weaving.impl;

import java.lang.annotation.Annotation;
import javassist.*;
import javassist.expr.ExprEditor;
import javassist.expr.FieldAccess;
import org.mosaicserver.util.Nonnull;

import static java.lang.String.format;

/**
 * @author arik
 */
class NullabilityWeaver extends Weaver
{
    @Override
    public void weave( @Nonnull CtClass ctClass ) throws ClassNotFoundException, NotFoundException, CannotCompileException
    {
        for( CtBehavior behavior : ctClass.getDeclaredBehaviors() )
        {
            weave( behavior );
        }
    }

    private void weave( @Nonnull CtBehavior behavior ) throws CannotCompileException, NotFoundException
    {
        if( Modifier.isAbstract( behavior.getModifiers() ) || Modifier.isNative( behavior.getModifiers() ) )
        {
            return;
        }

        // weave checks for @Nonnull parameters
        Object[][] availableParameterAnnotations = behavior.getAvailableParameterAnnotations();
        for( int i = 0; i < availableParameterAnnotations.length; i++ )
        {
            for( Object annotationObject : availableParameterAnnotations[ i ] )
            {
                Annotation annotation = ( Annotation ) annotationObject;
                if( annotation.annotationType().equals( Nonnull.class ) )
                {
                    behavior.insertBefore(
                            format(
                                    "if( $%d == null )                                                                                                     \n" +
                                    "{                                                                                                                     \n" +
                                    "  throw new NullPointerException( \"Method parameter '%s' of method '%s' is null, but is annotated with @Nonnull\" ); \n" +
                                    "}                                                                                                                     \n",
                                    i + 1, getParameterName( behavior, i ), behavior.getLongName()
                            )
                    );
                }
            }
        }

        // weave check for return type of @Nonnull methods
        if( behavior instanceof CtMethod )
        {
            CtMethod method = ( CtMethod ) behavior;
            if( method.hasAnnotation( Nonnull.class ) && !method.getReturnType().getName().equals( Void.class.getName() ) )
            {
                method.insertAfter( format(
                        "if( $_ == null )                                                                                       \n" +
                        "{                                                                                                      \n" +
                        "   throw new NullPointerException( \"Method '%s' returned null, but is annotated with @Nonnull\" );    \n" +
                        "}                                                                                                      \n",
                        method.getLongName()
                ) );
            }
        }

        // prevent assignment of null to @Nonnull fields (in this class or another class)
        behavior.instrument( new ExprEditor()
        {
            @Override
            public void edit( FieldAccess f ) throws CannotCompileException
            {
                if( f.isWriter() )
                {
                    CtField field;
                    try
                    {
                        field = f.getField();
                    }
                    catch( NotFoundException e )
                    {
                        throw new IllegalStateException( e );
                    }

                    CtClass type;
                    try
                    {
                        type = field.getType();
                    }
                    catch( NotFoundException e )
                    {
                        throw new IllegalStateException( e );
                    }

                    if( !type.isPrimitive() && field.hasAnnotation( Nonnull.class ) )
                    {
                        f.replace( format( "$0.%s = (%s) java.util.Objects.requireNonNull( $1, \"field '%s.%s' must not be null\" );",
                                           field.getName(),
                                           type.getName(),
                                           field.getDeclaringClass().getSimpleName(),
                                           field.getName() ) );
                    }
                }
            }
        } );
    }
}
