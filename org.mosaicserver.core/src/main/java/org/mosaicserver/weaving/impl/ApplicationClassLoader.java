package org.mosaicserver.weaving.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Collectors;
import javassist.*;
import org.mosaicserver.launch.Instance;
import org.mosaicserver.modules.Module;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.Nullable;
import org.mosaicserver.util.ToStringHelper;
import org.mosaicserver.weaving.impl.intercept.MethodInterceptionWeaver;
import throwing.bridge.ThrowingBridge;

import static java.util.Arrays.asList;
import static java.util.Collections.enumeration;
import static java.util.stream.Collectors.toList;
import static org.mosaicserver.util.Format.msg;

/**
 * @author arik
 */
public final class ApplicationClassLoader extends ClassLoader
{
    @Nonnull
    private final Instance instance;

    @Nonnull
    private final ClassPool classPool;

    @Nonnull
    private final List<Weaver> weavers;

    @Nonnull
    private final Set<String> delegateToParent = new CopyOnWriteArraySet<>();

    public ApplicationClassLoader( @Nonnull ClassLoader parent, @Nonnull Instance instance )
    {
        super( parent );
        this.instance = instance;
        this.classPool = createClassPool( parent );
        this.weavers = asList( new NullabilityWeaver(), new InjectionWeaver(), new MethodInterceptionWeaver() );
    }

    @Override
    public String toString()
    {
        return ToStringHelper.create( this ).toString();
    }

    @Nonnull
    public Instance getInstance()
    {
        return this.instance;
    }

    @Override
    protected Class<?> loadClass( @Nonnull String name, boolean resolve ) throws ClassNotFoundException
    {
        synchronized( getClassLoadingLock( name ) )
        {
            // First, check if the class has already been loaded
            Class<?> c = findLoadedClass( name );
            if( c == null )
            {
                if( this.delegateToParent.contains( name ) )
                {
                    // we already searched for this class-name, and came up short - delegate to parent (or throw CNFE)
                    c = findClassThroughDelegation( name );
                }
                else
                {
                    // check if any module actually contains this class
                    URL classResource = findResource( name.replace( '.', '/' ) + ".class" );
                    if( classResource != null )
                    {
                        // one of our modules contains it - lets load it
                        c = findClass( name );
                    }
                    else
                    {
                        // no module contains this class - delegate to parent
                        this.delegateToParent.add( name );
                        c = findClassThroughDelegation( name );
                    }
                }
            }
            if( resolve )
            {
                resolveClass( c );
            }
            return c;
        }
    }

    @Nonnull
    private Class<?> findClassThroughDelegation( @Nonnull String name ) throws ClassNotFoundException
    {
        ClassLoader parent = getParent();
        if( parent != null )
        {
            return parent.loadClass( name );
        }
        else
        {
            throw new ClassNotFoundException( msg( "could not find class '{}'", name ) );
        }
    }

    @Override
    @Nonnull
    protected Class<?> findClass( @Nonnull String name ) throws ClassNotFoundException
    {
        // obtain javassist class from the pool
        CtClass ctClass = findCtClass( name );

        // weave the class
        weaveCtClass( name, ctClass );

        // declare package
        definePackage( name );

        // compile and return
        return defineClass( name, ctClass );
    }

    @Nonnull
    private CtClass findCtClass( @Nonnull String name ) throws ClassNotFoundException
    {
        CtClass ctClass;
        try
        {
            ctClass = this.classPool.get( name );
        }
        catch( NotFoundException e )
        {
            throw new ClassNotFoundException( msg( "could not find class '{}' or one of its dependencies", name ), e );
        }
        return ctClass;
    }

    private void weaveCtClass( @Nonnull String name, @Nonnull CtClass ctClass ) throws ClassNotFoundException
    {
        if( !ctClass.isArray() && !ctClass.isAnnotation() && !ctClass.isInterface() )
        {
            try
            {
                for( Weaver weaver : this.weavers )
                {
                    weaver.weave( ctClass );
                }
            }
            catch( CannotCompileException e )
            {
                throw new IllegalStateException( msg( "could not weave class '{}'", name ), e );
            }
            catch( ClassNotFoundException e )
            {
                throw e;
            }
            catch( Exception e )
            {
                throw new IllegalStateException( msg( "internal error while weaving class '{}'", name ), e );
            }
        }
    }

    private void definePackage( @Nonnull String name )
    {
        int lastDotIndex = name.lastIndexOf( '.' );
        if( lastDotIndex >= 0 )
        {
            String packageName = name.substring( 0, lastDotIndex );
            if( getPackage( packageName ) == null )
            {
                try
                {
                    definePackage( packageName, null, null, null, null, null, null, null );
                }
                catch( IllegalArgumentException e )
                {
                    // ignore.  maybe the package object for the same
                    // name has been created just right away.
                }
            }
        }
    }

    @Nonnull
    private Class<?> defineClass( @Nonnull String name, @Nonnull CtClass ctClass )
    {
        byte[] bytes;
        try
        {
            bytes = ctClass.toBytecode();
        }
        catch( IOException | CannotCompileException e )
        {
            throw new IllegalStateException( msg( "could not compile class '{}'", name ), e );
        }
        return defineClass( name, bytes, 0, bytes.length );
    }

    @Override
    @Nullable
    protected URL findResource( @Nullable String name )
    {
        return this.instance.getModuleManager()
                            .getModules()
                            .map( module -> findResourceInModule( module, name ) )
                            .filter( Optional::isPresent )
                            .map( Optional::get )
                            .findFirst().orElse( null );
    }

    @Override
    @Nonnull
    protected Enumeration<URL> findResources( @Nullable String name ) throws IOException
    {
        return enumeration( this.instance.getModuleManager()
                                         .getModules()
                                         .map( module -> findResourcesInModule( module, name ) )
                                         .flatMap( urls -> urls.stream() )
                                         .collect( toList() ) );
    }

    @Nonnull
    private Optional<URL> findResourceInModule( @Nonnull Module module, @Nullable String name )
    {
        return module.findResource( Objects.requireNonNull( name ) ).map( path -> {
            try
            {
                return path.toUri().toURL();
            }
            catch( MalformedURLException e )
            {
                throw new IllegalStateException( msg( "could not convert path '{}' to URL", path ) );
            }
        } );
    }

    @Nonnull
    private List<URL> findResourcesInModule( @Nonnull Module module, @Nullable String name )
    {
        return module.findResources( Objects.requireNonNull( name ) ).stream().map( path -> {
            try
            {
                return path.toUri().toURL();
            }
            catch( MalformedURLException e )
            {
                throw new IllegalStateException( msg( "could not convert path '{}' to URL", path ) );
            }
        } ).collect( Collectors.toList() );
    }

    @Nonnull
    private ClassPool createClassPool( @Nonnull ClassLoader parent )
    {
        ClassPool classPool = new ClassPool();
        classPool.appendSystemPath();
        classPool.appendClassPath( new LoaderClassPath( parent ) );
        try
        {
            ThrowingBridge.of( this.instance.getModuleManager().getModules(), NotFoundException.class )
                          .map( Module::getExplodedPath )
                          .map( path -> path.toAbsolutePath().toString() )
                          .map( path -> path.endsWith( "/" ) ? path.substring( 0, path.length() - 1 ) : path )
                          .forEach( classPool::appendClassPath );
        }
        catch( NotFoundException e )
        {
            throw new IllegalStateException( "could not create application class-pool", e );
        }
        return classPool;
    }
}
