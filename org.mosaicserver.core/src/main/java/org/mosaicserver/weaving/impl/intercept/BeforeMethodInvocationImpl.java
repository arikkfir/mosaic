package org.mosaicserver.weaving.impl.intercept;

import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.Nullable;
import org.mosaicserver.weaving.MethodInterceptor;

/**
* @author arik
*/
class BeforeMethodInvocationImpl extends AbstractMethodInvocation
        implements MethodInterceptor.BeforeMethodInvocation
{
    BeforeMethodInvocationImpl( @Nonnull InvocationContext context )
    {
        super( context );
    }

    @Nonnull
    @Override
    public Object[] getArguments()
    {
        return this.context.getArguments();
    }

    @Nonnull
    @Override
    public MethodInterceptor.BeforeInvocationDecision continueInvocation()
    {
        return MethodEntry.continueDecision;
    }

    @Nonnull
    @Override
    public MethodInterceptor.BeforeInvocationDecision abort( @Nullable Object returnValue )
    {
        this.context.setReturnValue(  returnValue );
        return MethodEntry.abortDecision;
    }
}
