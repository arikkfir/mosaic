package org.mosaicserver.weaving.impl.intercept;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.Nullable;
import org.mosaicserver.weaving.MethodInterceptor;

/**
 * @author arik
 */
class InvocationContext extends HashMap<String, Object>
{
    @Nonnull
    private final MethodEntry methodEntry;

    @Nullable
    private final Object object;

    @Nonnull
    private final Object[] arguments;

    @Nonnull
    private final List<InterceptorRef> invokedInterceptors = new LinkedList<>();

    @Nullable
    private Map<InterceptorRef, Map<String, Object>> interceptorInvocationContexts = null;

    @Nullable
    private BeforeMethodInvocationImpl beforeInvocation;

    @Nullable
    private AfterMethodInvocationImpl afterInvocation;

    @Nullable
    private AfterMethodExceptionImpl afterThrowable;

    @Nullable
    private Throwable throwable;

    @Nullable
    private Object returnValue;

    InvocationContext( @Nonnull MethodEntry methodEntry, @Nullable Object object, @Nonnull Object[] arguments )
    {
        super( 5 );
        this.methodEntry = methodEntry;
        this.object = object;
        this.arguments = arguments;
    }

    @Nonnull
    MethodEntry getMethodEntry()
    {
        return this.methodEntry;
    }

    @Nullable
    Object getObject()
    {
        return this.object;
    }

    @Nonnull
    Object[] getArguments()
    {
        return this.arguments;
    }

    @Nullable
    Object getReturnValue()
    {
        return this.returnValue;
    }

    void setReturnValue( @Nullable Object returnValue )
    {
        this.returnValue = returnValue;
    }

    @Nullable
    Throwable getThrowable()
    {
        return this.throwable;
    }

    void setThrowable( @Nullable Throwable throwable )
    {
        this.throwable = throwable;
    }

    boolean beforeInvocation()
    {
        for( InterceptorRef interceptorRef : this.methodEntry.getMethodInterceptors() )
        {
            try
            {
                BeforeMethodInvocationImpl invocation = getBeforeInvocation();
                invocation.interceptorRef = interceptorRef;

                // invoke interceptor
                MethodInterceptor.BeforeInvocationDecision decision = interceptorRef.getInterceptor().beforeInvocation( invocation );

                // if interceptor succeeded, add it to the list of interceptors to invoke on "after" action
                // note that we add it to the start, so we can invoke them in reverse order in "after" action
                this.invokedInterceptors.add( 0, interceptorRef );

                if( decision == MethodEntry.abortDecision )
                {
                    return false;
                }
                else if( decision != MethodEntry.continueDecision )
                {
                    throw new IllegalStateException( "method interceptor \"before\" did not use MethodInvocation.continue/abort methods" );
                }
            }
            catch( Throwable throwable )
            {
                this.throwable = throwable;
                this.returnValue = null;
                return false;
            }
        }
        return true;
    }

    @Nullable
    Object afterInvocation() throws Throwable
    {
        for( InterceptorRef interceptorRef : this.invokedInterceptors )
        {
            try
            {
                if( this.throwable != null )
                {
                    AfterMethodExceptionImpl invocation = getAfterThrowable();
                    invocation.interceptorRef = interceptorRef;
                    this.returnValue = interceptorRef.getInterceptor().afterThrowable( invocation );
                }
                else
                {
                    AfterMethodInvocationImpl invocation = getAfterInvocation();
                    invocation.interceptorRef = interceptorRef;
                    this.returnValue = interceptorRef.getInterceptor().afterInvocation( invocation );
                }
                this.throwable = null;
            }
            catch( Throwable throwable )
            {
                this.throwable = throwable;
                this.returnValue = null;
            }
        }

        if( this.throwable != null )
        {
            throw this.throwable;
        }
        else
        {
            return this.returnValue;
        }
    }

    @Nonnull
    BeforeMethodInvocationImpl getBeforeInvocation()
    {
        if( this.beforeInvocation == null )
        {
            this.beforeInvocation = new BeforeMethodInvocationImpl( this );
        }
        return this.beforeInvocation;
    }

    @Nonnull
    AfterMethodInvocationImpl getAfterInvocation()
    {
        if( this.afterInvocation == null )
        {
            this.afterInvocation = new AfterMethodInvocationImpl( this );
        }
        return this.afterInvocation;
    }

    @Nonnull
    AfterMethodExceptionImpl getAfterThrowable()
    {
        if( this.afterThrowable == null )
        {
            this.afterThrowable = new AfterMethodExceptionImpl( this );
        }
        return this.afterThrowable;
    }

    @Nonnull
    Map<String, Object> getInterceptorInvocationContext( @Nonnull InterceptorRef interceptorEntry )
    {
        if( this.interceptorInvocationContexts == null )
        {
            this.interceptorInvocationContexts = new HashMap<>();
        }

        Map<String, Object> context = this.interceptorInvocationContexts.get( interceptorEntry );
        if( context == null )
        {
            context = new HashMap<>();
            this.interceptorInvocationContexts.put( interceptorEntry, context );
        }
        return context;
    }
}
