package org.mosaicserver.weaving.impl.intercept;

import java.util.HashMap;
import java.util.Map;
import javassist.*;
import javassist.bytecode.AccessFlag;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.weaving.MethodInterceptor;
import org.mosaicserver.weaving.impl.Weaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;
import static javassist.Modifier.*;
import static javassist.bytecode.AccessFlag.BRIDGE;
import static javassist.bytecode.AccessFlag.SYNTHETIC;

/**
 * @author arik
 */
public class MethodInterceptionWeaver extends Weaver
{
    @Override
    public void weave( @Nonnull CtClass ctClass )
            throws ClassNotFoundException, NotFoundException, CannotCompileException
    {
        if( isSubtypeOf( ctClass, MethodInterceptor.class.getName() ) )
        {
            Logger logger = LoggerFactory.getLogger( ctClass.getName() );
            logger.info( "Class '{}' will not be weaved for method interception, because it implements {}",
                         ctClass.getName(), MethodInterceptor.class.getName() );
            return;
        }

        // creates a shared map of method entries for this class
        Map<CtMethod, Long> methodIds = createClassInitializer( ctClass );

        // weave methods for interception
        for( CtMethod method : ctClass.getDeclaredMethods() )
        {
            Long id = methodIds.get( method );
            if( id != null )
            {
                weaveMethodForInterception( id, method );
            }
        }
    }

    @Nonnull
    private Map<CtMethod, Long> createClassInitializer( @Nonnull CtClass ctClass ) throws CannotCompileException
    {
        CtConstructor classInitializer = ctClass.getClassInitializer();
        if( classInitializer == null )
        {
            classInitializer = ctClass.makeClassInitializer();
        }
        ctClass.addField( CtField.make( "public static final java.util.Map __METHOD_ENTRIES = new java.util.HashMap(100);", ctClass ) );

        // this counter will increment for each declared method in this class
        // ie. each methods receives a unique ID (in the context of this class..)
        long id = 0;

        // iterate declared methods, and for each method, add code that populates the __METHOD_ENTRIES static map
        // with a MethodEntry for that method. The entry will receive the method's unique ID.
        // in addition to generating the code to populate the class's method entries map, we save the method IDs
        // mapping in a local map here and return it - so that the code weaved to our methods will use that id to
        // fetch method entry on runtime when methods are invoked.

        StringBuilder methodIdMapSrc = new StringBuilder();
        Map<CtMethod, Long> methodIds = new HashMap<>();
        for( CtMethod method : ctClass.getDeclaredMethods() )
        {
            int acc = AccessFlag.of( method.getModifiers() );
            if( ( acc & BRIDGE ) == 0 && ( acc & SYNTHETIC ) == 0 )
            {
                int modifiers = method.getModifiers();
                if( !isAbstract( modifiers ) && !isNative( modifiers ) )
                {
                    long methodId = id++;
                    methodIdMapSrc.append(
                            format(
                                    "%s.__METHOD_ENTRIES.put(                   \n" +
                                    "   Long.valueOf( %dl ),                    \n" +
                                    "   new %s( %s.class, \"%s\", $sig )   \n" +
                                    ");                                         \n",
                                    ctClass.getName(),
                                    methodId,
                                    MethodEntry.class.getName(),
                                    method.getDeclaringClass().getName(),
                                    method.getName()
                            )
                    );
                    methodIds.put( method, methodId );
                }
            }
        }
        classInitializer.insertBefore( "{\n" + methodIdMapSrc.toString() + "}" );
        return methodIds;
    }

    private void weaveMethodForInterception( long id, @Nonnull CtMethod method )
            throws NotFoundException, CannotCompileException
    {
        // add code that invokes the 'after' interception
        String afterSuccessSrc = format(
                "{                                                                                                          \n" +
                "   %s __myEntry = (%s) __METHOD_ENTRIES.get( Long.valueOf( %dl ) );                                        \n" +
                "   " + getReturnStatement( method.getReturnType(), "__myEntry.afterSuccessfulInvocation( ($w)$_ )" ) + "   \n" +
                "}                                                                                                          \n",
                MethodEntry.class.getName(),
                MethodEntry.class.getName(),
                id
        );
        method.insertAfter( afterSuccessSrc, false );

        // add code that catches exceptions
        String catchSrc = format(
                "{                                                                                          \n" +
                "   %s __myEntry = (%s) __METHOD_ENTRIES.get( Long.valueOf( %dl ) );                        \n" +
                "   " + getReturnStatement( method.getReturnType(), "__myEntry.afterThrowable( $e )" ) + "  \n" +
                "   throw $e;                                                                               \n" +
                "}                                                                                          \n",
                MethodEntry.class.getName(),
                MethodEntry.class.getName(),
                id
        );
        method.addCatch( catchSrc, method.getDeclaringClass().getClassPool().get( Throwable.class.getName() ), "$e" );

        // add code that invokes the 'before' interception
        String beforeSrc = format(
                "{                                                                      \n" +
                "   %s __myEntry = (%s) __METHOD_ENTRIES.get( Long.valueOf( %dl ) );    \n" +
                "   if( !__myEntry.beforeInvocation( %s, $args ) )                      \n" +
                "   {                                                                   \n" +
                "       %s;                                                             \n" +
                "   }                                                                   \n" +
                "}                                                                      \n",
                MethodEntry.class.getName(),
                MethodEntry.class.getName(),
                id,
                isStatic( method.getModifiers() ) ? "null" : "this",
                getReturnStatement( method.getReturnType(), "__myEntry.afterAbortedInvocation()" )
        );
        method.insertBefore( beforeSrc );

        // add code that invokes the 'after' interception
        String afterSrc = format(
                "{                                                                      \n" +
                "   %s __myEntry = (%s) __METHOD_ENTRIES.get( Long.valueOf( %dl ) );    \n" +
                "   __myEntry.cleanup();                                                \n" +
                "}                                                                      \n",
                MethodEntry.class.getName(),
                MethodEntry.class.getName(),
                id
        );
        method.insertAfter( afterSrc, true );
    }

    private boolean isSubtypeOf( @Nonnull CtClass type, @Nonnull String superTypeName ) throws NotFoundException
    {
        if( type.getName().equals( superTypeName ) )
        {
            return true;
        }

        for( String interfaceName : type.getClassFile2().getInterfaces() )
        {
            try
            {
                if( interfaceName.equals( MethodInterceptor.class.getName() )
                    || isSubtypeOf( type.getClassPool().get( interfaceName ), superTypeName ) )
                {
                    return true;
                }
            }
            catch( NotFoundException ignore )
            {
                // simply not weaving the class; it won't load anyway...
            }
        }

        String superName = type.getClassFile2().getSuperclass();
        return superName != null && isSubtypeOf( type.getClassPool().get( superName ), superTypeName );
    }

    @Nonnull
    private String getReturnStatement( @Nonnull CtClass returnType, @Nonnull String valueStmt )
    {
        switch( returnType.getName() )
        {
            case "void":
                return valueStmt + ";";
            case "boolean":
                return format( "return ( (Boolean) %s ).booleanValue();", valueStmt );
            case "byte":
                return format( "return ( (Number) %s ).byteValue();", valueStmt );
            case "char":
                return format( "return ( (Character) %s ).charValue();", valueStmt );
            case "double":
                return format( "return ( (Number) %s ).doubleValue();", valueStmt );
            case "float":
                return format( "return ( (Number) %s ).floatValue();", valueStmt );
            case "int":
                return format( "return ( (Number) %s ).intValue();", valueStmt );
            case "long":
                return format( "return ( (Number) %s ).longValue();", valueStmt );
            case "short":
                return format( "return ( (Number) %s ).shortValue();", valueStmt );
            default:
                return format( "return (%s) %s;", returnType.getName(), valueStmt );
        }
    }
}
