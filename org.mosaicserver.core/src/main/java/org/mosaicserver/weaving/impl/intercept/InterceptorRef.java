package org.mosaicserver.weaving.impl.intercept;

import java.util.Map;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.weaving.MethodInterceptor;

/**
* @author arik
*/
class InterceptorRef
{
    @Nonnull
    private final MethodInterceptor interceptor;

    @Nonnull
    private final Map<String, Object> context;

    InterceptorRef( @Nonnull MethodInterceptor interceptor, @Nonnull Map<String, Object> context )
    {
        this.interceptor = interceptor;
        this.context = context;
    }

    @Nonnull
    MethodInterceptor getInterceptor()
    {
        return this.interceptor;
    }

    @Nonnull
    Map<String, Object> getContext()
    {
        return this.context;
    }
}
