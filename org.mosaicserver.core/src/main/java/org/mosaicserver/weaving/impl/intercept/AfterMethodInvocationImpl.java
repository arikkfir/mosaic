package org.mosaicserver.weaving.impl.intercept;

import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.Nullable;
import org.mosaicserver.weaving.MethodInterceptor;

import static java.util.Arrays.copyOf;

/**
* @author arik
*/
class AfterMethodInvocationImpl extends AbstractMethodInvocation
        implements MethodInterceptor.AfterMethodInvocation
{
    @Nonnull
    private final Object[] arguments;

    AfterMethodInvocationImpl( @Nonnull InvocationContext context )
    {
        super( context );

        Object[] arguments = this.context.getArguments();
        this.arguments = copyOf( arguments, arguments.length );
    }

    @Nonnull
    @Override
    public Object[] getArguments()
    {
        return this.arguments;
    }

    @Nullable
    @Override
    public Object getReturnValue()
    {
        return this.context.getReturnValue();
    }
}
