package org.mosaicserver.weaving.impl;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javassist.*;
import org.mosaicserver.components.Component;
import org.mosaicserver.components.impl.ComponentManagerImpl;
import org.mosaicserver.util.Nonnull;
import throwing.bridge.ThrowingBridge;

import static javassist.Modifier.isStatic;

/**
 * @author arik
 */
class InjectionWeaver extends Weaver
{
    @Override
    public void weave( @Nonnull CtClass ctClass )
            throws ClassNotFoundException, NotFoundException, CannotCompileException
    {
        Collection<CtConstructor> superCallingConstructors =
                ThrowingBridge.of( Stream.of( ctClass.getDeclaredConstructors() ), CannotCompileException.class )
                              .filter( CtConstructor::callsSuper )
                              .collect( Collectors.toList() );

        for( CtField field : ctClass.getDeclaredFields() )
        {
            int modifiers = field.getModifiers();
            if( !isStatic( modifiers ) )
            {
                if( field.hasAnnotation( Component.class ) )
                {
                    String statement = String.format( "this.%s = (%s) %s.getValueForField( %s.class, \"%s\" );",
                                                      field.getName(),
                                                      field.getType().getName(),
                                                      ComponentManagerImpl.class.getName(),
                                                      ctClass.getName(),
                                                      field.getName() );
                    for( CtConstructor constructor : superCallingConstructors )
                    {
                        constructor.insertBeforeBody( statement );
                    }
                }
            }
        }
    }
}
