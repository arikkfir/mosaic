package org.mosaicserver.components.impl;

import com.fasterxml.classmate.ResolvedType;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.mosaicserver.components.PreShutdown;
import org.mosaicserver.launch.Instance;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.Nullable;
import org.mosaicserver.util.ToStringHelper;
import org.slf4j.Logger;

import static org.mosaicserver.util.Format.msg;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 */
class ComponentHolder extends TypeHolder
{
    private static final Logger LOG = getLogger( ComponentHolder.class );

    @Nonnull
    private final List<Method> preShutdownMethods;

    @Nullable
    private Object componentInstance;

    ComponentHolder( @Nonnull Instance instance, @Nonnull ResolvedType componentType )
    {
        this( instance, componentType, null );
    }

    ComponentHolder( @Nonnull Instance instance,
                     @Nonnull ResolvedType componentType,
                     @Nullable Object componentInstance )
    {
        super( instance, componentType );
        this.componentInstance = componentInstance;

        // only validate our type if no instance was passed (so built-in objects escape these validations)
        if( this.componentInstance == null )
        {
            if( componentType.isAbstract() )
            {
                throw new IllegalArgumentException( msg( "@Component cannot be used on abstract classes (found on '{}')", componentType ) );
            }
            else if( componentType.isInterface() )
            {
                throw new IllegalArgumentException( msg( "@Component cannot be used on interfaces (found on '{}')", componentType ) );
            }
            else if( componentType.getErasedType().isEnum() )
            {
                throw new IllegalArgumentException( msg( "@Component cannot be used on enums (found on '{}')", componentType ) );
            }
            else if( componentType.getErasedType().isAnnotation() )
            {
                throw new IllegalArgumentException( msg( "@Component cannot be used on annotations (found on '{}')", componentType ) );
            }
            else
            {
                try
                {
                    componentType.getErasedType().getDeclaredConstructor().setAccessible( true );
                }
                catch( NoSuchMethodException e )
                {
                    throw new IllegalArgumentException( msg( "@Component cannot be used on classes with no default constructor (encountered on '{}')", componentType ) );
                }
            }
        }

        // detect @PostStartup and @PreShutdown methods
        List<Method> preShutdownMethods = new LinkedList<>();
        Class<?> type = getType().getErasedType();
        while( type != null )
        {
            preShutdownMethods.addAll(
                    Stream.of( type.getDeclaredMethods() )
                          .filter( method -> method.isAnnotationPresent( PreShutdown.class ) )
                          .filter( method -> method.getParameterCount() == 0 )
                          .collect( Collectors.toList() )
            );
            type = type.getSuperclass();
        }
        preShutdownMethods.forEach( method -> method.setAccessible( true ) );
        this.preShutdownMethods = preShutdownMethods;
    }

    @Override
    public String toString()
    {
        return ToStringHelper.create( this )
                             .add( "type", getType() )
                             .toString();
    }

    @Nonnull
    Object getComponentInstance()
    {
        if( !this.getInstance().getComponentManager().isStarted() )
        {
            throw new IllegalStateException( "server not started" );
        }
        else if( this.componentInstance == null )
        {
            synchronized( this )
            {
                // check again, this time synchronized to make sure no other thread is trying to do the same
                if( this.componentInstance == null )
                {
                    try
                    {
                        this.componentInstance = getType().getErasedType().newInstance();
                    }
                    catch( InstantiationException e )
                    {
                        throw new IllegalStateException( msg( "could not instantiate component of type '{}'", getType() ), e );
                    }
                    catch( IllegalAccessException e )
                    {
                        throw new IllegalStateException( msg( "could not access component of type '{}'", getType() ), e );
                    }
                }
            }
        }
        return this.componentInstance;
    }

    synchronized void start()
    {
        LOG.info( "Started component '{}' ({})", getType(), getComponentInstance() );
    }

    synchronized void stop()
    {
        Object componentInstance = this.componentInstance;
        if( componentInstance != null )
        {
            // discard
            this.componentInstance = null;

            // shutdown instance
            this.preShutdownMethods.forEach( method -> {
                try
                {
                    method.invoke( componentInstance );
                }
                catch( Throwable e )
                {
                    LOG.error( "@PreShutdown method '{}' of component '{}' failed", method.getName(), getType(), e );
                }
            } );
        }
    }
}
