package org.mosaicserver.components.impl;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.mosaicserver.components.Component;
import org.mosaicserver.launch.Instance;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.Nullable;
import org.mosaicserver.util.ToStringHelper;

import static java.util.Collections.*;
import static java.util.stream.Collectors.toSet;
import static org.mosaicserver.util.Format.msg;
import static org.mosaicserver.util.ReflectionUtil.isAssignableFrom;

/**
 * @author arik
 */
class TypeHolder
{
    @Nonnull
    private final Instance instance;

    @Nonnull
    private final ResolvedType type;

    @Nonnull
    private final Map<String, FieldInfo> dependencies = new LinkedHashMap<>( 5 );

    public TypeHolder( @Nonnull Instance instance, @Nonnull ResolvedType type )
    {
        this.instance = instance;
        this.type = type;
    }

    @Override
    public String toString()
    {
        return ToStringHelper.create( this )
                             .add( "type", this.type )
                             .toString();
    }

    @Nonnull
    Instance getInstance()
    {
        return this.instance;
    }

    @Nonnull
    ResolvedType getType()
    {
        return this.type;
    }

    @Nonnull
    Object getValueForField( @Nonnull String fieldName )
    {
        FieldInfo fieldInfo = this.dependencies.get( fieldName );
        if( fieldInfo == null )
        {
            throw new IllegalStateException( msg( "field '{}' in type '{}' is not managed", fieldName, this.type ) );
        }
        else
        {
            return fieldInfo.getValue();
        }
    }

    @Nonnull
    Set<TypeHolder> resolveDependencies( @Nonnull TypeResolver typeResolver, @Nonnull Set<TypeHolder> typeHolders )
    {
        Set<TypeHolder> dependencies = new LinkedHashSet<>();

        Class<?> currentType = this.type.getErasedType();
        while( currentType != null )
        {
            Stream.of( currentType.getDeclaredFields() )
                  .filter( field -> field.isAnnotationPresent( Component.class ) )
                  .flatMap( field -> {
                      if( field.getType().equals( Collection.class ) )
                      {
                          return resolveCollectionField( typeResolver, field, typeHolders ).stream();
                      }
                      else if( field.getType().equals( List.class ) )
                      {
                          return resolveListField( typeResolver, field, typeHolders ).stream();
                      }
                      else if( field.getType().equals( Set.class ) )
                      {
                          return resolveSetField( typeResolver, field, typeHolders ).stream();
                      }
                      else
                      {
                          return resolveSingletonField( typeResolver, field, typeHolders ).stream();
                      }
                  } )
                  .forEach( dependencies::add );
            currentType = currentType.getSuperclass();
        }

        return dependencies;
    }

    @Nonnull
    private Set<TypeHolder> resolveSingletonField( @Nonnull TypeResolver typeResolver,
                                                   @Nonnull Field field,
                                                   @Nonnull Collection<TypeHolder> typeHolders )
    {
        ResolvedType resolvedFieldType = typeResolver.resolve( field.getGenericType() );
        Optional<TypeHolder> component = typeHolders.stream()
                                                    .filter( typeHolder -> typeHolder instanceof ComponentHolder )
                                                    .filter( typeHolder -> isAssignableFrom( resolvedFieldType, typeResolver.resolve( typeHolder.getType() ) ) )
                                                    .findFirst();
        if( component.isPresent() )
        {
            Set<TypeHolder> matches = singleton( component.get() );
            this.dependencies.put( field.getName(), new SingletonFieldInfo( matches ) );
            return matches;
        }
        else
        {
            throw new IllegalStateException( msg( "could not find a component of type '{}' for field '{}' in class '{}'", resolvedFieldType, field.getName(), this.type ) );
        }
    }

    @Nonnull
    private Set<TypeHolder> resolveCollectionField( @Nonnull TypeResolver typeResolver,
                                                    @Nonnull Field field,
                                                    @Nonnull Collection<TypeHolder> typeHolders )
    {
        return resolveListField( typeResolver, field, typeHolders );
    }

    @Nonnull
    private Set<TypeHolder> resolveListField( @Nonnull TypeResolver typeResolver,
                                              @Nonnull Field field,
                                              @Nonnull Collection<TypeHolder> typeHolders )
    {
        Class<?> fieldType = field.getType();
        ResolvedType resolvedFieldType = typeResolver.resolve( field.getGenericType() );
        ResolvedType fieldCollectionType = getCollectionType( field, resolvedFieldType, fieldType );
        Set<TypeHolder> matches =
                typeHolders.stream()
                           .filter( typeHolder -> typeHolder instanceof ComponentHolder )
                           .filter( typeHolder -> isAssignableFrom( fieldCollectionType, typeHolder.getType() ) )
                           .collect( toSet() );
        this.dependencies.put( field.getName(), new ListFieldInfo( matches ) );
        return matches;
    }

    @Nonnull
    private Set<TypeHolder> resolveSetField( @Nonnull TypeResolver typeResolver,
                                             @Nonnull Field field,
                                             @Nonnull Collection<TypeHolder> typeHolders )
    {
        Class<?> fieldType = field.getType();
        ResolvedType resolvedFieldType = typeResolver.resolve( field.getGenericType() );
        ResolvedType fieldCollectionType = getCollectionType( field, resolvedFieldType, fieldType );
        Set<TypeHolder> matches =
                typeHolders.stream()
                           .filter( typeHolder -> typeHolder instanceof ComponentHolder )
                           .filter( typeHolder -> isAssignableFrom( fieldCollectionType, typeHolder.getType() ) )
                           .collect( toSet() );
        this.dependencies.put( field.getName(), new SetFieldInfo( matches ) );
        return matches;
    }

    @Nonnull
    private ResolvedType getCollectionType( @Nonnull Field field,
                                            @Nonnull ResolvedType resolvedType,
                                            @Nonnull Class<?> collectionType )
    {
        List<ResolvedType> typeParameters = resolvedType.typeParametersFor( collectionType );
        if( typeParameters.isEmpty() || typeParameters.size() != 1 )
        {
            throw new IllegalStateException( msg( "field '{}' in type '{}' is a collection dependency, but has no type parameters, or too many/few type parameters", field.getName(), this.type ) );
        }
        else
        {
            return typeParameters.get( 0 );
        }
    }

    static abstract class FieldInfo<T>
    {
        @Nonnull
        protected final Set<TypeHolder> typeHolders;

        @Nullable
        private T value;

        public FieldInfo( @Nonnull Set<TypeHolder> typeHolders )
        {
            this.typeHolders = typeHolders;
        }

        @Nonnull
        final T getValue()
        {
            if( this.value == null )
            {
                synchronized( this )
                {
                    if( this.value == null )
                    {
                        this.value = createValue();
                    }
                }
            }
            return this.value;
        }

        @Nonnull
        protected abstract T createValue();
    }

    private class SingletonFieldInfo extends FieldInfo<Object>
    {
        public SingletonFieldInfo( @Nonnull Set<TypeHolder> typeHolders )
        {
            super( typeHolders );
        }

        @Nonnull
        @Override
        protected Object createValue()
        {
            Optional<Object> optional = this.typeHolders.stream()
                                                        .filter( typeHolder -> typeHolder instanceof ComponentHolder )
                                                        .map( typeHolder -> ( ComponentHolder ) typeHolder )
                                                        .map( ComponentHolder::getComponentInstance )
                                                        .findFirst();
            if( optional.isPresent() )
            {
                return optional.get();
            }
            else
            {
                throw new IllegalStateException( "should not happen" );
            }
        }
    }

    private class ListFieldInfo extends FieldInfo<List<?>>
    {
        public ListFieldInfo( @Nonnull Set<TypeHolder> typeHolders )
        {
            super( typeHolders );
        }

        @Nonnull
        @Override
        protected List<?> createValue()
        {
            return unmodifiableList( this.typeHolders.stream()
                                                     .filter( typeHolder -> typeHolder instanceof ComponentHolder )
                                                     .map( typeHolder -> ( ComponentHolder ) typeHolder )
                                                     .map( ComponentHolder::getComponentInstance )
                                                     .collect( Collectors.toList() ) );
        }
    }

    private class SetFieldInfo extends FieldInfo<Set<?>>
    {
        public SetFieldInfo( @Nonnull Set<TypeHolder> typeHolders )
        {
            super( typeHolders );
        }

        @Nonnull
        @Override
        protected Set<?> createValue()
        {
            return unmodifiableSet( this.typeHolders.stream()
                                                    .filter( typeHolder -> typeHolder instanceof ComponentHolder )
                                                    .map( typeHolder -> ( ComponentHolder ) typeHolder )
                                                    .map( ComponentHolder::getComponentInstance )
                                                    .collect( Collectors.toSet() ) );
        }
    }
}
