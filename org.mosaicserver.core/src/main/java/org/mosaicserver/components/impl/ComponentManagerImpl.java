package org.mosaicserver.components.impl;

import com.fasterxml.classmate.TypeResolver;
import java.util.*;
import java.util.stream.Collectors;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.TopologicalOrderIterator;
import org.mosaicserver.components.Component;
import org.mosaicserver.components.ComponentManager;
import org.mosaicserver.launch.Instance;
import org.mosaicserver.modules.Module;
import org.mosaicserver.modules.ModuleManager;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.Nullable;
import org.mosaicserver.util.ToStringHelper;
import org.mosaicserver.weaving.impl.ApplicationClassLoader;
import org.slf4j.Logger;

import static java.util.Collections.unmodifiableCollection;
import static org.mosaicserver.util.Format.msg;
import static org.mosaicserver.util.ReflectionUtil.isAssignableFrom;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 */
public final class ComponentManagerImpl implements ComponentManager
{
    private static final Logger LOG = getLogger( ComponentManagerImpl.class );

    @SuppressWarnings( "UnusedDeclaration" )
    @Nullable
    public static Object getValueForField( @Nonnull Class<?> declaringClass, @Nonnull String fieldName )
    {
        ClassLoader classLoader = declaringClass.getClassLoader();
        if( classLoader instanceof ApplicationClassLoader )
        {
            ApplicationClassLoader appClassLoader = ( ApplicationClassLoader ) classLoader;
            ComponentManagerImpl componentManager = appClassLoader.getInstance().getComponentManager();
            if( !componentManager.isStarted() )
            {
                throw new IllegalStateException( "server not started" );
            }

            Collection<TypeHolder> types = componentManager.types;
            if( types == null )
            {
                throw new IllegalStateException( "server not started" );
            }

            Optional<TypeHolder> optional = types.stream()
                                                 .filter( typeHolder -> typeHolder.getType().getErasedType().equals( declaringClass ) )
                                                 .findFirst();
            if( !optional.isPresent() )
            {
                throw new IllegalStateException( msg( "could not find type '{}'", declaringClass.getName() ) );
            }

            TypeHolder typeHolder = optional.get();
            return typeHolder.getValueForField( fieldName );
        }
        else
        {
            throw new IllegalStateException( msg( "target type '{}' was not loaded by Mosaic", declaringClass ) );
        }
    }

    @Nonnull
    private final Instance instance;

    private volatile boolean started;

    @Nullable
    private Collection<TypeHolder> types;

    public ComponentManagerImpl( @Nonnull Instance instance )
    {
        this.instance = instance;
    }

    @Override
    public String toString()
    {
        return ToStringHelper.create( this ).toString();
    }

    public synchronized void start()
    {
        TypeResolver typeResolver = new TypeResolver();

        // create components list, with built-in components
        Set<TypeHolder> typeHolders = new LinkedHashSet<>();
        typeHolders.add( new ComponentHolder( this.instance, typeResolver.resolve( Instance.class ), this.instance ) );
        typeHolders.add( new ComponentHolder( this.instance, typeResolver.resolve( ModuleManager.class ), this.instance.getModuleManager() ) );
        typeHolders.add( new ComponentHolder( this.instance, typeResolver.resolve( ComponentManager.class ), this ) );

        // discover application components
        this.instance.getModuleManager()
                     .getModules()
                     .flatMap( Module::getClasses )
                     .map( clazz -> {
                         if( clazz.isAnnotationPresent( Component.class ) )
                         {
                             return new ComponentHolder( this.instance, typeResolver.resolve( clazz ) );
                         }
                         else
                         {
                             return new TypeHolder( this.instance, typeResolver.resolve( clazz ) );
                         }
                     } )
                     .forEach( typeHolders::add );

        // create component graph
        DefaultDirectedGraph<TypeHolder, DefaultEdge> graph = new DefaultDirectedGraph<>( DefaultEdge.class );
        typeHolders.forEach( graph::addVertex );

        // map all dependencies between components
        typeHolders.forEach( typeHolder -> {

            Set<TypeHolder> dependencies = typeHolder.resolveDependencies( typeResolver, typeHolders );
            dependencies.forEach( dependency -> graph.addEdge( typeHolder, dependency ) );

        } );

        // check for unresolvable dependencies
        CycleDetector<TypeHolder, DefaultEdge> cycleDetector = new CycleDetector<>( graph );
        if( cycleDetector.detectCycles() )
        {
            StringBuilder buffer = new StringBuilder( 1000 );

            Set<TypeHolder> cycleVertices = cycleDetector.findCycles();
            while( !cycleVertices.isEmpty() )
            {
                buffer.append( "    cycle: " );

                Iterator<TypeHolder> iterator = cycleVertices.iterator();
                TypeHolder cycle = iterator.next();
                for( TypeHolder sub : cycleDetector.findCyclesContainingVertex( cycle ) )
                {
                    buffer.append( " -> " ).append( sub );
                    cycleVertices.remove( sub );
                }
                buffer.append( "\n" );
            }

            throw new IllegalStateException( "component cycles detected!\n" + buffer );
        }

        // print component tree
        TopologicalOrderIterator<TypeHolder, DefaultEdge> orderIterator = new TopologicalOrderIterator<>( graph );
        while( orderIterator.hasNext() )
        {
            LOG.info( orderIterator.next().toString() );
        }

        // save
        this.types = unmodifiableCollection( typeHolders );
        this.started = true;

        // instantiate all components
        this.types.stream()
                  .filter( typeHolder -> typeHolder instanceof ComponentHolder )
                  .map( typeHolder -> ( ComponentHolder ) typeHolder )
                  .forEach( ComponentHolder::start );
    }

    public boolean isStarted()
    {
        return this.started;
    }

    public synchronized void stop()
    {
        boolean started = this.started;
        if( started )
        {
            this.started = false;

            Collection<TypeHolder> types = this.types;
            if( types != null )
            {
                types.stream()
                     .filter( typeHolder -> typeHolder instanceof ComponentHolder )
                     .map( typeHolder -> ( ComponentHolder ) typeHolder )
                     .forEach( ComponentHolder::stop );
                this.types = null;
            }
        }
    }

    @SuppressWarnings( "unchecked" )
    @Nonnull
    @Override
    public <T> Optional<? extends T> getComponent( @Nonnull Class<T> ofType )
    {
        Collection<TypeHolder> types = this.types;
        if( types == null )
        {
            throw new IllegalStateException( "server not started" );
        }
        else
        {
            return ( Optional<? extends T> ) types.stream()
                                                  .filter( typeHolder -> typeHolder instanceof ComponentHolder )
                                                  .map( typeHolder -> ( ComponentHolder ) typeHolder )
                                                  .filter( componentHolder -> isAssignableFrom( ofType, componentHolder.getType() ) )
                                                  .map( ComponentHolder::getComponentInstance )
                                                  .findFirst();
        }
    }

    @SuppressWarnings( "unchecked" )
    @Nonnull
    public <T> List<? extends T> getComponents( @Nonnull Class<T> ofType )
    {
        Collection<TypeHolder> types = this.types;
        if( types == null )
        {
            throw new IllegalStateException( "server not started" );
        }
        else
        {
            return ( List<? extends T> ) types.stream()
                                              .filter( typeHolder -> typeHolder instanceof ComponentHolder )
                                              .map( typeHolder -> ( ComponentHolder ) typeHolder )
                                              .filter( componentHolder -> isAssignableFrom( ofType, componentHolder.getType() ) )
                                              .map( ComponentHolder::getComponentInstance )
                                              .collect( Collectors.toList() );
        }
    }
}
