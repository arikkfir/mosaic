package org.mosaicserver.util;

import java.io.*;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.FileTime;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Stream;

import static java.nio.file.Files.exists;
import static java.nio.file.Files.isSymbolicLink;
import static java.nio.file.StandardOpenOption.*;

/**
 * @author arik
 */
public final class IoUtil
{
    @Nonnull
    public static Properties readProperties( @Nonnull Path path )
    {
        try( InputStream inputStream = Files.newInputStream( path, StandardOpenOption.READ ) )
        {
            Properties properties = new Properties();
            properties.load( inputStream );
            return properties;
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    @Nonnull
    public static Properties readProperties( @Nonnull URL url )
    {
        try( InputStream inputStream = url.openStream() )
        {
            Properties properties = new Properties();
            properties.load( inputStream );
            return properties;
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    @Nonnull
    public static String readString( @Nonnull URL url )
    {
        try( BufferedReader reader = new BufferedReader( new InputStreamReader( url.openStream(), "UTF-8" ) ) )
        {
            StringBuilder buffer = new StringBuilder( 100 );

            char[] chars = new char[ 1024 ];
            int read;
            while( ( read = reader.read( chars ) ) >= 0 )
            {
                buffer.append( chars, 0, read );
            }

            return buffer.toString();
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    @Nonnull
    public static List<String> readLines( @Nonnull URL url )
    {
        StringTokenizer tokenizer = new StringTokenizer( readString( url ), "\n", false );
        List<String> lines = new LinkedList<>();
        while( tokenizer.hasMoreTokens() )
        {
            lines.add( tokenizer.nextToken() );
        }
        return lines;
    }

    @Nonnull
    public static List<String> readLines( @Nonnull Path path )
    {
        StringTokenizer tokenizer = new StringTokenizer( readString( path ), "\n", false );
        List<String> lines = new LinkedList<>();
        while( tokenizer.hasMoreTokens() )
        {
            lines.add( tokenizer.nextToken() );
        }
        return lines;
    }

    @Nonnull
    public static String readString( @Nonnull Path path )
    {
        try
        {
            return new String( Files.readAllBytes( path ), "UTF-8" );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    @Nonnull
    public static FileTime getLastModifiedTime( @Nonnull Path path, @Nonnull LinkOption... linkOptions )
    {
        try
        {
            return Files.getLastModifiedTime( path, linkOptions );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    public static void writeString( @Nonnull Path path, @Nonnull String content )
    {
        try
        {
            Files.createDirectories( path.getParent() );
            Files.write( path, content.getBytes( "UTF-8" ), CREATE, TRUNCATE_EXISTING, WRITE );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    public static long readLong( @Nonnull Path path )
    {
        try
        {
            return Long.parseLong( new String( Files.readAllBytes( path ), "UTF-8" ) );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    @Nonnull
    public static Path readSymbolicLink( @Nonnull Path path )
    {
        try
        {
            return Files.readSymbolicLink( path );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    @Nonnull
    public static Path toRealPath( @Nonnull Path path )
    {
        try
        {
            return path.toRealPath();
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    public static void createSymbolicLink( @Nonnull Path link, @Nonnull Path target )
    {
        try
        {
            if( Files.exists( link, LinkOption.NOFOLLOW_LINKS ) )
            {
                Files.delete( link );
            }
            Files.createSymbolicLink( link, target );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    @Nonnull
    public static Path createTempDirectory( @Nonnull String prefix, @Nonnull FileAttribute<?>... attrs )
    {
        try
        {
            return Files.createTempDirectory( prefix, attrs );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    @Nonnull
    public static Path createTempDirectory( @Nonnull Path path,
                                            @Nonnull String prefix,
                                            @Nonnull FileAttribute<?>... attrs )
    {
        try
        {
            return Files.createTempDirectory( path, prefix, attrs );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    public static void createDirectories( @Nonnull Path... paths )
    {
        for( Path path : paths )
        {
            if( isSymbolicLink( path ) )
            {
                createDirectories( readSymbolicLink( path ) );
            }
            else if( exists( path ) )
            {
                if( Files.isRegularFile( path ) )
                {
                    throw new UncheckedIOException( new IOException( String.format( "path '%s' is a file and not a directory", path ) ) );
                }
            }
            else
            {
                try
                {
                    Files.createDirectories( path );
                }
                catch( IOException e )
                {
                    throw new UncheckedIOException( e );
                }
            }
        }
    }

    public static void delete( @Nonnull Path path )
    {
        if( Files.exists( path ) )
        {
            if( Files.isDirectory( path ) )
            {
                try
                {
                    Files.walkFileTree( path, new SimpleFileVisitor<Path>()
                    {
                        @Nonnull
                        @Override
                        public FileVisitResult visitFile( @Nonnull Path file, @Nonnull BasicFileAttributes attrs )
                                throws IOException
                        {
                            Files.delete( file );
                            return FileVisitResult.CONTINUE;
                        }

                        @Nonnull
                        @Override
                        public FileVisitResult postVisitDirectory( @Nonnull Path dir, IOException exc )
                                throws IOException
                        {
                            Files.delete( dir );
                            return FileVisitResult.CONTINUE;
                        }
                    } );
                }
                catch( IOException e )
                {
                    throw new UncheckedIOException( e );
                }
            }
            else
            {
                try
                {
                    Files.delete( path );
                }
                catch( IOException e )
                {
                    throw new UncheckedIOException( e );
                }
            }
        }
    }

    public static void extract( @Nonnull Path jarPath, @Nonnull Path targetPath )
    {
        createDirectories( targetPath );

        try( JarFile jarFile = new JarFile( jarPath.toFile(), true ) )
        {
            jarFile.stream().sorted( new JarEntriesComparator() ).forEach( entry -> {

                try
                {
                    Path target = targetPath.resolve( entry.getName() );
                    if( entry.isDirectory() )
                    {
                        Files.createDirectories( target );
                    }
                    else
                    {
                        Files.createDirectories( target.getParent() );
                        try( InputStream inputStream = jarFile.getInputStream( entry ) )
                        {
                            Files.copy( inputStream, target, StandardCopyOption.REPLACE_EXISTING );
                        }
                    }
                }
                catch( IOException e )
                {
                    throw new UncheckedIOException( e );
                }

            } );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    public static void copy( @Nonnull Path sourcePath, @Nonnull Path targetPath )
    {
        try
        {
            Files.walkFileTree( sourcePath, new SimpleFileVisitor<Path>()
            {
                @Nonnull
                @Override
                public FileVisitResult preVisitDirectory( @Nonnull Path dir, @Nonnull BasicFileAttributes attrs )
                        throws IOException
                {
                    Path relativePath = sourcePath.relativize( dir );
                    Path target = targetPath.resolve( relativePath );
                    Files.createDirectories( target );
                    return FileVisitResult.CONTINUE;
                }

                @Nonnull
                @Override
                public FileVisitResult visitFile( @Nonnull Path file, @Nonnull BasicFileAttributes attrs )
                        throws IOException
                {
                    Path relativePath = sourcePath.relativize( file );
                    Path target = targetPath.resolve( relativePath );
                    Files.copy( file, target, StandardCopyOption.REPLACE_EXISTING );
                    return FileVisitResult.CONTINUE;
                }
            } );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    public static Stream<Path> walk( Path start, FileVisitOption... options )
    {
        try
        {
            return Files.walk( start, options );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }
    }

    private static class JarEntriesComparator implements Comparator<JarEntry>
    {
        @Override
        public int compare( JarEntry o1, JarEntry o2 )
        {
            if( o1.isDirectory() && !o2.isDirectory() )
            {
                return -1;
            }
            else if( !o1.isDirectory() && o2.isDirectory() )
            {
                return 1;
            }
            else
            {
                return o1.getName().compareToIgnoreCase( o2.getName() );
            }
        }
    }
}
