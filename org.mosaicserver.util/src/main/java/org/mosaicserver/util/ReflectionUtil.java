package org.mosaicserver.util;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author arik
 */
public final class ReflectionUtil
{
    public static boolean isAssignableFrom( @Nonnull Type leftType, @Nonnull Type rightType )
    {
        TypeResolver typeResolver = new TypeResolver();
        ResolvedType left = typeResolver.resolve( leftType );
        ResolvedType right = typeResolver.resolve( rightType );

        Class<?> leftErasedType = left.getErasedType();
        Class<?> rightErasedType = right.getErasedType();

        if( !leftErasedType.isAssignableFrom( rightErasedType ) )
        {
            return false;
        }

        ResolvedType resolvedLeftTypeFromRight = right.findSupertype( leftErasedType );

        List<ResolvedType> leftTypeParams = left.getTypeParameters();
        List<ResolvedType> leftTypeParamsFromRight = resolvedLeftTypeFromRight.getTypeParameters();
        if( leftTypeParams.size() != leftTypeParamsFromRight.size() )
        {
            return false;
        }

        for( int i = 0; i < leftTypeParams.size(); i++ )
        {
            ResolvedType typeParamFromLeft = leftTypeParams.get( i );
            ResolvedType typeParamFromRight = leftTypeParamsFromRight.get( i );
            if( !isAssignableFrom( typeParamFromLeft, typeParamFromRight ) )
            {
                return false;
            }
        }

        return true;
    }

    private ReflectionUtil()
    {
    }
}
