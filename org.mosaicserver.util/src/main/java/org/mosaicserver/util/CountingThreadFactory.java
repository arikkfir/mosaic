package org.mosaicserver.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

/**
* @author arik
*/
public class CountingThreadFactory implements ThreadFactory
{
    @Nonnull
    private final String prefix;

    @Nonnull
    private final AtomicLong counter = new AtomicLong( 0 );

    public CountingThreadFactory( @Nonnull String prefix )
    {
        this.prefix = prefix;
    }

    @Override
    public Thread newThread( @Nonnull Runnable r )
    {
        return new Thread( r, this.prefix + "-" + this.counter.incrementAndGet() );
    }
}
