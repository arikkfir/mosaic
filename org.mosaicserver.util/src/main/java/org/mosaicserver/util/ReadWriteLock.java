package org.mosaicserver.util;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

/**
 * @author arik
 */
public class ReadWriteLock
{
    @Nonnull
    private final String name;

    private final long defaultTimeout;

    @Nonnull
    private final TimeUnit defaultTimeUnit;

    @Nonnull
    private final MosaicReentrantReadWriteLock lock = new MosaicReentrantReadWriteLock( false );

    private final boolean writeReentrant;

    public ReadWriteLock( @Nonnull String name )
    {
        this( name, true );
    }

    public ReadWriteLock( @Nonnull String name, boolean writeReentrant )
    {
        this( name, 30, TimeUnit.SECONDS, writeReentrant );
    }

    public ReadWriteLock( @Nonnull String name, long defaultTimeout, @Nonnull TimeUnit defaultTimeUnit )
    {
        this( name, defaultTimeout, defaultTimeUnit, true );
    }

    public ReadWriteLock( @Nonnull String name,
                          long defaultTimeout,
                          @Nonnull TimeUnit defaultTimeUnit,
                          boolean writeReentrant )
    {
        this.name = name;
        this.defaultTimeout = defaultTimeout;
        this.defaultTimeUnit = defaultTimeUnit;
        this.writeReentrant = writeReentrant;
    }

    public void acquireReadLock()
    {
        acquireReadLock( this.defaultTimeout, this.defaultTimeUnit );
    }

    public void acquireReadLock( long timeout, @Nonnull TimeUnit timeUnit )
    {
        try
        {
            if( !this.lock.readLock().tryLock( timeout, timeUnit ) )
            {
                StringBuilder msg = new StringBuilder( 1000 );

                msg.append( "could not acquire read lock '" ).append( this.name ).append( "' for " ).append( timeout ).append( " " ).append( timeUnit ).append( "\n" );
                msg.append( "\n" );

                Thread owner = this.lock.getOwner();
                if( owner == null )
                {
                    msg.append( "No thread holding the write lock is!\n" );
                }
                else
                {
                    msg.append( "Thread holding the write lock is: " ).append( owner.getName() ).append( " [" ).append( owner.getId() ).append( "]\n" );
                    for( StackTraceElement traceElement : owner.getStackTrace() )
                    {
                        msg.append( "\tat " ).append( traceElement );
                    }
                }
                throw new LockException( msg.toString() );
            }
        }
        catch( InterruptedException e )
        {
            throw new LockException( "interrupted while trying to acquire read lock '" + this.name + "' for " + timeout + " " + timeUnit );
        }
    }

    public void releaseReadLock()
    {
        this.lock.readLock().unlock();
    }

    public void acquireWriteLock()
    {
        acquireWriteLock( this.defaultTimeout, this.defaultTimeUnit );
    }

    public void acquireWriteLock( long timeout, @Nonnull TimeUnit timeUnit )
    {
        if( !this.writeReentrant )
        {
            throw new IllegalStateException( "cannot reacquire write-lock on '" + this.name + "' - not write-reentrant" );
        }

        try
        {
            if( !this.lock.writeLock().tryLock( timeout, timeUnit ) )
            {
                StringBuilder msg = new StringBuilder( 1000 );

                msg.append( "could not acquire write lock '" ).append( this.name ).append( "' for " ).append( timeout ).append( " " ).append( timeUnit ).append( "\n" );
                msg.append( "\n" );

                Thread owner = this.lock.getOwner();
                if( owner == null )
                {
                    msg.append( "No thread holding the write lock is!\n" );
                }
                else
                {
                    msg.append( "Thread holding the write lock is: " ).append( owner.getName() ).append( " [" ).append( owner.getId() ).append( "]\n" );
                    for( StackTraceElement traceElement : owner.getStackTrace() )
                    {
                        msg.append( "\tat " ).append( traceElement );
                    }
                }
                throw new LockException( msg.toString() );
            }
        }
        catch( InterruptedException e )
        {
            throw new LockException( "could not acquire write lock '" + this.name + "' for " + timeout + " " + timeUnit );
        }
    }

    public void releaseWriteLock()
    {
        this.lock.writeLock().unlock();
    }

    public <T> T read( @Nonnull Callable<T> function )
    {
        return read( function, this.defaultTimeout, this.defaultTimeUnit );
    }

    public <T> T read( @Nonnull Callable<T> function, long timeout, @Nonnull TimeUnit timeUnit )
    {
        acquireReadLock( timeout, timeUnit );
        try
        {
            return function.call();
        }
        catch( RuntimeException e )
        {
            throw e;
        }
        catch( Exception e )
        {
            throw new RuntimeException( e );
        }
        finally
        {
            releaseReadLock();
        }
    }

    public void read( @Nonnull Runnable function )
    {
        read( function, this.defaultTimeout, this.defaultTimeUnit );
    }

    public void read( @Nonnull Runnable function, long timeout, @Nonnull TimeUnit timeUnit )
    {
        acquireReadLock( timeout, timeUnit );
        try
        {
            function.run();
        }
        catch( RuntimeException e )
        {
            throw e;
        }
        catch( Exception e )
        {
            throw new RuntimeException( e );
        }
        finally
        {
            releaseReadLock();
        }
    }

    public <T> T write( @Nonnull Callable<T> function )
    {
        return write( function, this.defaultTimeout, this.defaultTimeUnit );
    }

    public <T> T write( @Nonnull Callable<T> function, long timeout, @Nonnull TimeUnit timeUnit )
    {
        int readHoldCount = this.lock.getReadHoldCount();
        while( this.lock.getReadHoldCount() > 0 )
        {
            releaseReadLock();
        }

        try
        {
            acquireWriteLock( timeout, timeUnit );
            try
            {
                return function.call();
            }
            catch( RuntimeException e )
            {
                throw e;
            }
            catch( Exception e )
            {
                throw new RuntimeException( e );
            }
            finally
            {
                releaseWriteLock();
            }
        }
        finally
        {
            for( int i = 0; i < readHoldCount; i++ )
            {
                acquireReadLock();
            }
        }
    }

    public void write( @Nonnull Runnable function )
    {
        write( function, this.defaultTimeout, this.defaultTimeUnit );
    }

    public void write( @Nonnull Runnable function, long timeout, @Nonnull TimeUnit timeUnit )
    {
        int readHoldCount = this.lock.getReadHoldCount();
        while( this.lock.getReadHoldCount() > 0 )
        {
            releaseReadLock();
        }

        try
        {
            acquireWriteLock( timeout, timeUnit );
            try
            {
                function.run();
            }
            catch( RuntimeException e )
            {
                throw e;
            }
            catch( Exception e )
            {
                throw new RuntimeException( e );
            }
            finally
            {
                releaseWriteLock();
            }
        }
        finally
        {
            for( int i = 0; i < readHoldCount; i++ )
            {
                acquireReadLock();
            }
        }
    }

    public <T> T obtainWithoutLocks( @Nonnull Supplier<T> future )
    {
        // remember the hold count we currently have for the read & write locks
        int readHoldCount = this.lock.getReadHoldCount();
        int writeHoldCount = this.lock.getWriteHoldCount();

        // release all lock holdings
        while( this.lock.getWriteHoldCount() > 0 )
        {
            releaseWriteLock();
        }
        while( this.lock.getReadHoldCount() > 0 )
        {
            releaseReadLock();
        }

        // park until the future is complete
        T result = null;
        RuntimeException exception = null;
        try
        {
            result = future.get();
        }
        catch( RuntimeException e )
        {
            exception = e;
        }

        // re-acquire previously held locks
        for( int i = 0; i < writeHoldCount; i++ )
        {
            acquireWriteLock();
        }
        for( int i = 0; i < readHoldCount; i++ )
        {
            acquireReadLock();
        }

        // if an exception was raised, re-throw it; otherwise, return the result
        if( exception != null )
        {
            throw exception;
        }
        else
        {
            return result;
        }
    }

    private class MosaicReentrantReadWriteLock extends ReentrantReadWriteLock
    {
        private MosaicReentrantReadWriteLock( boolean fair )
        {
            super( fair );
        }

        @Override
        public Thread getOwner()
        {
            return super.getOwner();
        }

        @Override
        public Collection<Thread> getQueuedReaderThreads()
        {
            return super.getQueuedReaderThreads();
        }

        @Override
        public Collection<Thread> getQueuedThreads()
        {
            return super.getQueuedThreads();
        }

        @Override
        public Collection<Thread> getQueuedWriterThreads()
        {
            return super.getQueuedWriterThreads();
        }
    }
}
