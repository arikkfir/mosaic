package org.mosaicserver.util;

import java.util.Map;
import javax.script.*;

/**
 * @author arik
 */
public class Filter
{
    @Nonnull
    private static final ScriptEngineManager SCRIPT_ENGINE_MANAGER = new ScriptEngineManager( Filter.class.getClassLoader() );

    @Nonnull
    private final String expression;

    @Nonnull
    private final CompiledScript compiledScript;

    public Filter( @Nonnull String expression )
    {
        this.expression = expression;

        ScriptEngine scriptEngine = SCRIPT_ENGINE_MANAGER.getEngineByName( "nashorn" );
        try
        {
            this.compiledScript = ( ( Compilable ) scriptEngine ).compile( expression );
        }
        catch( ScriptException e )
        {
            throw new IllegalArgumentException( "could not compile '" + expression + "'", e );
        }
    }

    @Override
    public String toString()
    {
        return "Filter[" + this.expression + "]";
    }

    public boolean matches( @Nonnull Map<String, Object> properties )
    {
        SimpleBindings bindings = new SimpleBindings( properties );
        try
        {
            Object result = this.compiledScript.eval( bindings );
            if( result == null )
            {
                return false;
            }
            else if( result instanceof Boolean )
            {
                return ( Boolean ) result;
            }
            else
            {
                return false;
            }
        }
        catch( ScriptException e )
        {
            throw new IllegalArgumentException( "could not evaluate '" + expression + "'", e );
        }
    }
}
