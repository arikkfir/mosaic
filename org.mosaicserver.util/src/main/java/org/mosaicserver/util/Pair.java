package org.mosaicserver.util;

/**
 * @author arik
 */
public class Pair<L, R>
{
    @Nullable
    private final L left;

    @Nullable
    private final R right;

    public Pair( @Nullable L left, @Nullable R right )
    {
        this.left = left;
        this.right = right;
    }

    @Nullable
    public L getLeft()
    {
        return this.left;
    }

    @Nullable
    public R getRight()
    {
        return this.right;
    }
}
