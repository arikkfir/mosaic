package org.mosaicserver.util;

/**
 * @author arik
 */
public final class StringUtil
{
    @Nonnull
    public static String repeat( @Nonnull String token, int count )
    {
        StringBuilder buffer = new StringBuilder();
        for( int i = 0; i < count; i++ )
        {
            buffer.append( token );
        }
        return buffer.toString();
    }

    @Nonnull
    public static String rightPad( @Nonnull String line, char token, int count )
    {
        StringBuilder buffer = new StringBuilder( line );
        while( buffer.length() < count )
        {
            buffer.append( token );
        }
        return buffer.toString();
    }
}
