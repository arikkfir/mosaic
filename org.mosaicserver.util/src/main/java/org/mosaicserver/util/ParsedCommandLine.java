package org.mosaicserver.util;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mosaicserver.util.Format.msg;

/**
 * @author arik
 */
public class ParsedCommandLine
{
    @Nonnull
    private final List<String> arguments = new LinkedList<>();

    @Nonnull
    private final List<Pair<String, String>> options = new LinkedList<>();

    public ParsedCommandLine( @Nonnull String commandLine )
    {
        String optionName = null;
        CliParseState state = CliParseState.NEUTRAL;
        CharStream stream = new CharStream( commandLine );
        while( stream.hasNext() )
        {
            char c = stream.next();
            switch( state )
            {
                case NEUTRAL:
                {
                    if( c != ' ' )
                    {
                        if( c != '-' )
                        {
                            // start reading an argument - read it fully
                            this.arguments.add( c + stream.readUntil( ' ' ) );
                        }
                        else if( !stream.hasNext() )
                        {
                            // looks like start of an option, but since that's the last char, it's treated as an argument
                            // eg. "mycommand arg1 -"
                            this.arguments.add( "-" );
                        }
                        else
                        {
                            state = CliParseState.POST_1ST_HYPHEN;
                        }
                    }
                    break;
                }
                case POST_1ST_HYPHEN:
                {
                    if( c == ' ' )
                    {
                        // a space following a hyphen is no option, treat as a an argument
                        // eg. "mycommand arg1 - arg2"
                        this.arguments.add( "-" );
                        state = CliParseState.NEUTRAL;
                    }
                    else if( c == '-' )
                    {
                        if( stream.hasNext() )
                        {
                            // another hyphen, it's probably a long option
                            state = CliParseState.POST_2ND_HYPHEN;
                        }
                        else
                        {
                            // stream ends here with a double hyphen - just add as an argument
                            // eg. "command arg1 --"
                            this.arguments.add( "--" );
                            state = CliParseState.NEUTRAL;
                        }
                    }
                    else
                    {
                        // a real option?
                        optionName = c + stream.readWhileNotAnyOf( " =:" );
                        state = CliParseState.POST_OPTION_NAME;
                    }
                    break;
                }
                case POST_2ND_HYPHEN:
                {
                    if( c == ' ' )
                    {
                        // a space following double hyphen is no option, treat as an argument
                        this.arguments.add( "--" );
                        state = CliParseState.NEUTRAL;
                    }
                    else if( c == '-' )
                    {
                        // 3rd hyphen! treat as an argument
                        this.arguments.add( "---" + stream.readUntil( ' ' ) );
                        state = CliParseState.NEUTRAL;
                    }
                    else
                    {
                        optionName = c + stream.readWhileNotAnyOf( " =:" );
                        state = CliParseState.POST_OPTION_NAME;
                    }
                    break;
                }
                case POST_OPTION_NAME:
                {
                    if( c == ' ' )
                    {
                        // space after the option name - user just gave us "-p " (mind the space after the "p")
                        // add a null value (signaling no value for this option instance) and return to neutral state
                        this.options.add( new Pair<>( optionName, null ) );
                        state = CliParseState.NEUTRAL;
                    }
                    else
                    {
                        // we know it's either "=" or ":" -> see call to "readWhileNotAnyOf in previous state
                        state = CliParseState.EXPECT_OPTION_VALUE;
                    }
                    break;
                }
                case EXPECT_OPTION_VALUE:
                {
                    if( c != ' ' )
                    {
                        if( c == '"' || c == '\'' )
                        {
                            this.options.add( new Pair<>( optionName, stream.readUntil( c ) ) );
                        }
                        else
                        {
                            this.options.add( new Pair<>( optionName, c + stream.readUntil( ' ' ) ) );
                        }
                    }

                    state = CliParseState.NEUTRAL;
                    break;
                }
            }

        }
        if( state == CliParseState.POST_OPTION_NAME || state == CliParseState.EXPECT_OPTION_VALUE )
        {
            this.options.add( new Pair<>( optionName, null ) );
        }
    }

    @Nonnull
    public List<String> getArguments()
    {
        return Collections.unmodifiableList( this.arguments );
    }

    @Nullable
    public String getArgumentValue( int index )
    {
        if( index >= this.arguments.size() )
        {
            return null;
        }
        else
        {
            return this.arguments.get( index );
        }
    }

    @Nonnull
    public Optional<String> singleValued( @Nonnull String... names )
    {
        Optional<String> holder = Optional.empty();
        Set<String> nameSet = Stream.of( names ).sorted().collect( Collectors.toSet() );
        for( Pair<String, String> option : this.options )
        {
            if( nameSet.contains( option.getLeft() ) )
            {
                if( holder.isPresent() )
                {
                    throw new IllegalArgumentException( msg( "option \"{}\" can only accept a single value", names[ 0 ] ) );
                }
                else if( option.getRight() == null )
                {
                    holder = Optional.of( "" );
                }
                else
                {
                    holder = Optional.of( option.getRight() );
                }
            }
        }
        return holder;
    }

    @Nonnull
    public List<String> multiValued( @Nonnull String... names )
    {
        List<String> values = null;
        Set<String> nameSet = Stream.of( names ).sorted().collect( Collectors.toSet() );
        for( Pair<String, String> option : this.options )
        {
            if( nameSet.contains( option.getLeft() ) )
            {
                String value = option.getRight();
                if( value != null && !value.trim().isEmpty() )
                {
                    if( values == null )
                    {
                        values = new LinkedList<>();
                    }
                    values.add( value );
                }
            }
        }
        return values == null ? Collections.<String>emptyList() : values;
    }

    public boolean hasOption( @Nonnull String... names )
    {
        Set<String> nameSet = Stream.of( names ).sorted().collect( Collectors.toSet() );
        for( Pair<String, String> option : this.options )
        {
            if( nameSet.contains( option.getLeft() ) )
            {
                String value = option.getRight();
                if( value != null && !value.trim().isEmpty() )
                {
                    throw new IllegalArgumentException( msg( "option \"{}\" cannot accept values", names[ 0 ] ) );
                }
                else
                {
                    return true;
                }
            }
        }
        return false;
    }

    private static enum CliParseState
    {
        NEUTRAL,
        POST_1ST_HYPHEN,
        POST_2ND_HYPHEN,
        POST_OPTION_NAME,
        EXPECT_OPTION_VALUE
    }
}
