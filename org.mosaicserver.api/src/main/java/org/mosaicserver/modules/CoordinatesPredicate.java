package org.mosaicserver.modules;

import java.util.function.Predicate;
import java.util.regex.Pattern;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.VersionRange;

/**
 * @author arik
 */
public class CoordinatesPredicate implements Predicate<Module>
{
    @Nonnull
    private final Pattern groupId;

    @Nonnull
    private final Pattern artifactId;

    @Nonnull
    private final VersionRange versionRange;

    public CoordinatesPredicate( @Nonnull String groupIdPattern, @Nonnull String artifactIdPattern )
    {
        this( groupIdPattern, artifactIdPattern, VersionRange.valueOf( "[,)" ) );
    }

    public CoordinatesPredicate( @Nonnull String groupIdPattern,
                                 @Nonnull String artifactIdPattern,
                                 @Nonnull VersionRange versionRange )
    {
        this.groupId = Pattern.compile( groupIdPattern );
        this.artifactId = Pattern.compile( artifactIdPattern );
        this.versionRange = versionRange;
    }

    public Pattern getGroupId()
    {
        return this.groupId;
    }

    public Pattern getArtifactId()
    {
        return this.artifactId;
    }

    public VersionRange getVersionRange()
    {
        return this.versionRange;
    }

    @Override
    public boolean test( Module module )
    {
        return this.groupId.matcher( module.getGroupId() ).matches() &&
               this.artifactId.matcher( module.getArtifactId() ).matches() &&
               this.versionRange.includes( module.getVersion() );
    }
}
