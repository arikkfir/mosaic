package org.mosaicserver.modules;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;
import org.mosaicserver.util.Nonnull;
import org.mosaicserver.util.Version;

/**
 * @author arik
 */
public interface Module
{
    @Nonnull
    String getGroupId();

    @Nonnull
    String getArtifactId();

    @Nonnull
    Version getVersion();

    @Nonnull
    Path getOriginalPath();

    @Nonnull
    Path getExplodedPath();

    @Nonnull
    Optional<Path> findResource( @Nonnull String pattern );

    @Nonnull
    Collection<Path> findResources( @Nonnull String pattern );

    @Nonnull
    Optional<? extends Class<?>> getClass( @Nonnull String className );

    @Nonnull
    Stream<? extends Class<?>> getClasses();
}
