package org.mosaicserver.modules;

import java.util.function.Predicate;
import java.util.stream.Stream;
import org.mosaicserver.util.Nonnull;

/**
 * @author arik
 */
public interface ModuleManager
{
    @Nonnull
    Stream<? extends Module> getModules();

    @Nonnull
    Stream<? extends Module> getModules( @Nonnull Predicate<Module> predicate );
}
