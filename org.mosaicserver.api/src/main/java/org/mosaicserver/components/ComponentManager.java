package org.mosaicserver.components;

import java.util.Collection;
import java.util.Optional;
import org.mosaicserver.util.Nonnull;

/**
 * @author arik
 */
public interface ComponentManager
{
    @Nonnull
    <T> Optional<? extends T> getComponent( @Nonnull Class<T> ofType );

    @Nonnull
    <T> Collection<? extends T> getComponents( @Nonnull Class<T> ofType );
}
